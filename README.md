**OSSMONEY**

OssMoney is an Open Source (http://opensource.org/licenses/mit-license.php) expense tracker built using Java (Spring, JPA) and Javascript (Angular, Bootstrap).

You can get an idea of the kind of things that OssMoney is capable of tracking seeing the screenshots at https://es.pinterest.com/jnoheda/ossmoney/

It's meant to be deployed in a server and receive multiple connections from different users while also allowing local installation an execution for single use in a home PC

Right now, OssMoney is under heavy development. You can nonetheless try it by downloading the sources and running the project from Eclipse or using Maven from a shell (beware, by default it uses an in memory database so all changes will be lost upon restarting)