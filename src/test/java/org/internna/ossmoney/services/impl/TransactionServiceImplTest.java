package org.internna.ossmoney.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Optional;
import java.math.BigDecimal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.internna.ossmoney.AbstractSecuredTest;
import org.internna.ossmoney.OssMoneyApplication;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.services.AccountService;
import org.internna.ossmoney.services.TransactionService;
import org.internna.ossmoney.model.transaction.Transaction;
import org.internna.ossmoney.model.transaction.TransactionDTO;
import org.internna.ossmoney.model.transaction.TransactionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OssMoneyApplication.class)
public class TransactionServiceImplTest extends AbstractSecuredTest {

    private @Autowired AccountService accountService;
    private @Autowired TransactionService transactionService;

    @Test(expected = IllegalArgumentException.class)
    public void testFind() {
        assertThat(transactionService.find(null).isPresent(), is(false));
        assertThat(transactionService.find("8483").isPresent(), is(false));
        assertThat(transactionService.find("1").isPresent(), is(true));
        loginAsTestUser();
        transactionService.find("1");
    }

    @Test
    public void testIncomeAndExpenses() {
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -3);
        final Iterable<Transaction> incomeAndExpenses = transactionService.incomeAndExpenses(calendar.getTime());
        assertThat(incomeAndExpenses, is(notNullValue()));
        Iterator<Transaction> iterator = incomeAndExpenses.iterator();
        Transaction transaction = iterator.next();
        assertThat(transaction.getTransactionType(), is(equalTo(TransactionType.EXPENSE)));
        assertThat(transaction.getAmount().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("-500"))));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void testObtainTransactions() {
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -16);
        List<Transaction> transactions = transactionService.obtainTransactions("2", calendar.getTime(), new Date());
        assertThat(transactions, hasSize(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDelete() {
        assertThat(transactionService.delete(null).isPresent(), is(false));
        assertThat(transactionService.delete("273123").isPresent(), is(false));
        final Optional<Account> deleted = transactionService.delete("15");
        assertThat(deleted.isPresent(), is(true));
        assertThat(deleted.get().getCurrentBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("625"))));
        loginAsTestUser();
        transactionService.delete("1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreate() {
        TransactionDTO dto = new TransactionDTO();
        dto.setAccount("5");
        dto.setMemo("Description");
        dto.setAmount(BigDecimal.TEN);
        dto.setTags(new String[] {"CINEMA"});
        Optional<Transaction> created = transactionService.save(dto);
        assertThat(created.isPresent(), is(true));
        assertThat(created.get().getTags(), hasSize(1));
        assertThat(created.get().getAccount().getCurrentBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal(560))));
        dto.setChargeAccount("1");
        dto.setAmount(new BigDecimal("-50"));
        dto.setTransactionType(TransactionType.TRANSFER);
        created = transactionService.save(dto);
        assertThat(created.isPresent(), is(true));
        assertThat(created.get().getTags(), hasSize(2));
        assertThat(created.get().getAccount().getCurrentBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal(510))));
        assertThat(accountService.find("1").get().getCurrentBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("11114.50"))));
        dto.setAmount(new BigDecimal("-100"));
        dto.setTags(new String[] {"CINEMA", "MUSIC", "TRANSFER"});
        created = transactionService.save(dto);
        assertThat(created.isPresent(), is(true));
        assertThat(created.get().getTags(), hasSize(3));
        assertThat(created.get().getAccount().getCurrentBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal(410))));
        assertThat(accountService.find("1").get().getCurrentBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("11214.50"))));
        loginAsTestUser();
        transactionService.save(dto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdate() {
        TransactionDTO dto = new TransactionDTO();
        dto.setUuid("17");
        dto.setAccount("5");
        dto.setMemo("Description");
        dto.setAmount(new BigDecimal("-400"));
        dto.setTags(new String[] {"CAR_INSURANCE", "CAR_MAINTENANCE"});
        Optional<Transaction> created = transactionService.save(dto);
        assertThat(created.isPresent(), is(true));
        assertThat(created.get().getTags(), hasSize(2));
        assertThat(created.get().getAccount().getCurrentBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("1621.21"))));
        loginAsTestUser();
        transactionService.save(dto);
    }
}
