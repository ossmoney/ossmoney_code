package org.internna.ossmoney.services.impl;

import java.util.Date;
import java.util.Optional;
import java.math.BigDecimal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.internna.ossmoney.AbstractSecuredTest;
import org.internna.ossmoney.OssMoneyApplication;
import org.internna.ossmoney.services.InvestmentService;
import org.internna.ossmoney.model.investment.deposit.Deposit;
import org.internna.ossmoney.model.investment.deposit.DepositDTO;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OssMoneyApplication.class)
public class InvestmentServiceImplTest extends AbstractSecuredTest {

    private @Autowired InvestmentService investmentService;

    @Test
    public void testRetrievePortfolio() {
        final InvestmentPortfolio portfolio = investmentService.retrievePortfolio();
        assertThat(portfolio, is(notNullValue()));
        assertThat(portfolio.getDeposits(), hasSize(2));
        assertThat(portfolio.getPreciousMetals(), hasSize(1));
        assertThat(portfolio.getDeposits().iterator().next().getTransactions(), hasSize(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindDeposit() {
        Optional<Deposit> deposit = investmentService.findDeposit(null);
        assertThat(deposit.isPresent(), is(false));
        deposit = investmentService.findDeposit("23423");
        assertThat(deposit.isPresent(), is(false));
        deposit = investmentService.findDeposit("2");
        assertThat(deposit.isPresent(), is(true));
        assertThat(deposit.get().getTransactions(), hasSize(2));
        loginAsTestUser();
        investmentService.findDeposit("1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCancel() {
        final DepositDTO dto = new DepositDTO();
        Optional<Deposit> deposit = investmentService.cancel(dto);
        assertThat(deposit.isPresent(), is(false));
        dto.setUuid("3242");
        deposit = investmentService.cancel(dto);
        assertThat(deposit.isPresent(), is(false));
        dto.setUuid("2");
        dto.setOpened(new Date());
        dto.setBalance(new BigDecimal("-100"));
        deposit = investmentService.cancel(dto);
        assertThat(deposit.isPresent(), is(true));
        assertThat(deposit.get().getTransactions(), hasSize(3));
        assertThat(deposit.get().getBalance().getNumber().numberValue(BigDecimal.class), is(equalTo(new BigDecimal("400"))));
        dto.setBalance(new BigDecimal("-1000"));
        deposit = investmentService.cancel(dto);
        assertThat(deposit.isPresent(), is(false));
        loginAsTestUser();
        investmentService.cancel(dto);
    }
}
