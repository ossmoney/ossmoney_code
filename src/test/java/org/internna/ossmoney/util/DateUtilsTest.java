package org.internna.ossmoney.util;

import org.junit.Test;
import java.util.Date;
import java.util.Calendar;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DateUtilsTest {

    @Test
    public void testDates() {
        Date[] dates = DateUtils.dates(5);
        assertEquals("Six months", 6, dates.length);
        Calendar calendar = Calendar.getInstance();
        Calendar datesCalendar = Calendar.getInstance();
        datesCalendar.setTime(dates[0]);
        calendar.add(Calendar.MONTH, -5);
        assertEquals("Six months later", calendar.get(Calendar.MONTH), datesCalendar.get(Calendar.MONTH));
        assertEquals("Last millisecond", new Integer(999), new Integer(datesCalendar.get(Calendar.MILLISECOND)));
        datesCalendar.setTime(dates[2]);
        assertEquals("All are set to last millisecond", new Integer(999), new Integer(datesCalendar.get(Calendar.MILLISECOND)));
    }

    @Test
    public void testGetMonthStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 0);
        Calendar init = Calendar.getInstance();
        init.setTime(DateUtils.getMonthStartDate(calendar.getTime()));
        assertEquals("Same year", calendar.get(Calendar.YEAR), init.get(Calendar.YEAR));
        assertEquals("Same month", calendar.get(Calendar.MONTH), init.get(Calendar.MONTH));
        assertEquals("Start of the month", 1, init.get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void testNextDate() {
        Date now = new Date();
        assertNotNull("Always return a date", DateUtils.nextDate(null));
        assertTrue("Always after", DateUtils.nextDate(now).after(now));
    }

    @Test
    @SuppressWarnings("deprecation")
    public void testGetRandomTimeDate() {
    	Date now = new Date();
    	assertNull("Null date is returned", DateUtils.getRandomTimeDate(null));
        assertNotNull("Always return a date for a date", DateUtils.getRandomTimeDate(now));
        assertFalse("Always return a different date", DateUtils.getRandomTimeDate(now).equals(now));
        assertEquals("In the same day", now.getDate(), DateUtils.getRandomTimeDate(now).getDate());
    }

    @Test
    @SuppressWarnings("deprecation")
    public void testGetMidnight() {
    	Date now = new Date();
    	assertNull("Null date is returned", DateUtils.getRandomTimeDate(null));
        assertNotNull("Always return a date for a date", DateUtils.getRandomTimeDate(now));
        assertFalse("Always return a different date", DateUtils.getRandomTimeDate(now).equals(now));
        assertTrue("Midnight", DateUtils.getMidnight(now).getHours() == 0);
    }

    @Test
    public void testAdd() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.set(Calendar.DAY_OF_YEAR, 350);
    	Calendar result = Calendar.getInstance();
		result.setTime(DateUtils.add(calendar.getTime(), 10));
    	assertEquals("Days added", new Integer(360), new Integer(result.get(Calendar.DAY_OF_YEAR)));
    }

    @Test
    public void testGetHour() {
    	assertTrue("Always return a positive hour", DateUtils.getHour(null) > 0);
    	Calendar calendar = Calendar.getInstance();
    	calendar.set(Calendar.HOUR_OF_DAY, 14);
    	assertEquals("Hour of day", new Integer(14), new Integer(DateUtils.getHour(calendar.getTime())));
    	calendar.set(Calendar.HOUR_OF_DAY, 25);
    	assertTrue("Always in range", DateUtils.getHour(calendar.getTime()) > 0 && DateUtils.getHour(calendar.getTime()) < 24);
    }

    @Test
    public void testIsToday() {
    	Calendar calendar = Calendar.getInstance();
    	assertTrue("Today", DateUtils.isToday(calendar.getTime()));
    	calendar.add(Calendar.DAY_OF_YEAR, 1);
    	assertFalse("Not today", DateUtils.isToday(calendar.getTime()));
    }

    @Test
    public void testGetMonths() {
    	Calendar calendar = Calendar.getInstance();
    	Calendar otherCalendar = Calendar.getInstance();
    	assertEquals("Same month", new Integer(0), new Integer(DateUtils.getMonths(calendar.getTime(), otherCalendar.getTime())));
    	calendar.set(Calendar.MONTH, 2);    	
    	otherCalendar.set(Calendar.MONTH, 8);
    	assertEquals("Six months", new Integer(6), new Integer(DateUtils.getMonths(calendar.getTime(), otherCalendar.getTime())));
    	otherCalendar.set(Calendar.MONTH, 1);
    	assertEquals("Eleven months", new Integer(11), new Integer(DateUtils.getMonths(calendar.getTime(), otherCalendar.getTime())));
    }

    @Test
    public void testAsQIF() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.set(Calendar.MONTH, 10);
    	calendar.set(Calendar.YEAR, 2011);
    	calendar.set(Calendar.DAY_OF_MONTH, 24);
    	assertEquals("QIF date", "D24/11'2011", DateUtils.asQIF(calendar.getTime()));
    	calendar.set(Calendar.DAY_OF_MONTH, 5);
    	assertEquals("Single day", "D05/11'2011", DateUtils.asQIF(calendar.getTime()));
    	calendar.set(Calendar.MONTH, 3);
    	assertEquals("Single day", "D05/04'2011", DateUtils.asQIF(calendar.getTime()));
    }

    @Test
    public void testInterval() {
    	Calendar calendar = Calendar.getInstance();
    	Date origin = calendar.getTime();
    	calendar.add(Calendar.DAY_OF_YEAR, 5);
    	assertEquals("No days in the middle", new Integer(0), new Integer(DateUtils.interval(null, calendar.getTime()).length));
    	assertEquals("Four days in the middle", new Integer(4), new Integer(DateUtils.interval(origin, calendar.getTime()).length));
    }

    @Test
    public void testGetBeginningOfTime() {
    	Calendar calendar = Calendar.getInstance();
    	assertTrue("Provided date is in the past", calendar.getTime().after(DateUtils.getBeginningOfTime()));
    }

}
