package org.internna.ossmoney;

import org.junit.Before;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.beans.factory.annotation.Autowired;

import static org.internna.ossmoney.model.security.User.DEMO_USER;

public abstract class AbstractSecuredTest {

    private static final String TEST_USER = "test";

    private @Autowired SignInAdapter singInAdapter;

    @Before
    public void login() {
        singInAdapter.signIn(DEMO_USER, null, null);
    }

    protected final void loginAsTestUser() {
        singInAdapter.signIn(AbstractSecuredTest.TEST_USER, null, null);
    }

}
