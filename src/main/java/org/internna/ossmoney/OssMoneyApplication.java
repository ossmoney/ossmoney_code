package org.internna.ossmoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OssMoneyApplication {

	public static void main(final String[] args) {
		SpringApplication.run(OssMoneyApplication.class, args);
	}

}
