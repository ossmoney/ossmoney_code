package org.internna.ossmoney.security;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.beans.factory.annotation.Autowired;

public final class SocialConnectionSignUp implements ConnectionSignUp {

	@Autowired private SignInAdapter signInAdapter; 

	@Override public String execute(final Connection<?> connection) {
		return signInAdapter.signIn(connection.getKey().toString(), connection, null);
	}

}
