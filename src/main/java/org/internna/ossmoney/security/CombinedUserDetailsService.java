package org.internna.ossmoney.security;

import org.internna.ossmoney.data.UserRepository;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.security.CombinedUser;
import org.springframework.dao.DataAccessException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public final class CombinedUserDetailsService implements SocialUserDetailsService, UserDetailsService {

	@Autowired private UserRepository userRepository;

	@Override public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		return loadUserByUserId(username);
	}

	public SocialUserDetails loadUserByUserId(final String username) throws UsernameNotFoundException, DataAccessException {
		final User user = userRepository.findOne(username);
		if (user != null) {
			return new CombinedUser(user);
		} else {
			throw new UsernameNotFoundException("No user found with username: " + username);
		}
	}

}
