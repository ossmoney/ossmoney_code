package org.internna.ossmoney.util;

import java.util.Locale;
import java.math.BigDecimal;
import java.text.NumberFormat;
import javax.money.MonetaryAmount;

public final class MoneyUtils {

    private static final NumberFormat formatter = NumberFormat.getNumberInstance(Locale.US);

    static {
        formatter.setMinimumFractionDigits(2);
        formatter.setMaximumFractionDigits(2);
    }

    private MoneyUtils() {
        throw new AssertionError("Do not try to instantiate utility class");
    }

    /**
     * Formats the number in US locale (1,517.85)
     *
     * @param amount any
     * @return any
     */
    public static String format(final MonetaryAmount amount) {
        return amount != null ? formatter.format(amount.getNumber().doubleValueExact()) : "0.00";
    }

    public static boolean equals(final MonetaryAmount amount, final MonetaryAmount otherAmount) {
        return ((amount == null) | (otherAmount == null)) ? false : amount.equals(otherAmount);
    }

    public static boolean equals(final MonetaryAmount amount, final BigDecimal balance) {
        return ((amount == null) | (balance == null)) ? false : amount.getNumber().numberValue(BigDecimal.class).equals(balance);
    }

}
