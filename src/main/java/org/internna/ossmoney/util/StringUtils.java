package org.internna.ossmoney.util;

import java.io.InputStream;
import java.text.Normalizer;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

public final class StringUtils {

    public static final String EMPTY = "";

    /**
     * Default private constructor for a Utility class.
     */
    private StringUtils() {
        throw new AssertionError("Do not try to instantiate utility class");
    }

    public static boolean hasText(final String text) {
        return text != null && text.trim().length() > 0;
    }

    public static InputStream asStream(final String origin) {
        String value = origin != null && origin.trim().length() > 0 ? origin : EMPTY;
        try {
            return new ByteArrayInputStream(value.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            return new ByteArrayInputStream(new byte[0]);
        }
    }

    public static String stripUTF(final String text) {
        final String normal = Normalizer.normalize(hasText(text) ? text : StringUtils.EMPTY, Normalizer.Form.NFD);
        return normal.replaceAll("[\\p{InCombiningDiacriticalMarks}]", StringUtils.EMPTY);
    }
}
