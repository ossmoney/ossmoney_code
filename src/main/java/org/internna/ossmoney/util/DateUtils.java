package org.internna.ossmoney.util;

import java.util.List;
import java.util.Date;
import java.util.Random;
import java.util.Calendar;
import java.util.ArrayList;
import java.text.SimpleDateFormat;

/**
 * Utilities to work with Dates.
 *
 */
public final class DateUtils {

    private static final Random random = new Random();
    private static final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM''yyyy");

    /**
     * Default private constructor for a Utility class.
     */
    private DateUtils() {
        throw new AssertionError("Do not try to instantiate utility class");
    }

    public static Date[] dates(final int months) {
        final Date[] dates = new Date[months + 1];
        Calendar calendar = Calendar.getInstance();
        dates[months] = end(calendar).getTime();
        for (int month = months; month > 0; month--) {
            calendar.add(Calendar.MONTH, -1);
            calendar = getMonthEndDate(calendar);
            dates[month - 1] = calendar.getTime();
        }
        return dates;
    }

    public static Calendar end(final Calendar calendar) {
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar;
    }

    public static Date getMonthEndDate(final Date month) {
        final Calendar calendar = Calendar.getInstance();
        if (month != null) {
            calendar.setTime(month);
        }
        return getMonthEndDate(calendar).getTime();
    }

    public static Calendar getMonthEndDate(final Calendar calendar) {
        final Calendar cal = calendar != null ? calendar : Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return end(cal);
    }

    public static Date getMonthStartDate(final Date month) {
        final Calendar calendar = Calendar.getInstance();
        if (month != null) {
            calendar.setTime(month);
        }
        getMonthStartDate(calendar);
        return calendar.getTime();
    }

    public static Calendar getMonthStartDate(final Calendar calendar) {
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return getMidnight(calendar);
    }

    /**
     * Obtains a date just after the provided one.
     *
     * @param date any
     * @return a non null instance
     */
    public static Date nextDate(final Date date) {
        final Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTime(date);
            calendar.add(Calendar.MILLISECOND, 1);
        }
        return calendar.getTime();
    }

    /**
     * Sets a random time for a given date.
     *
     * @param operationDate any
     * @return any
     */
    public static Date getRandomTimeDate(final Date operationDate) {
        if (operationDate != null) {
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(operationDate);
            calendar.set(Calendar.SECOND, random.nextInt(59));
            calendar.set(Calendar.MINUTE, random.nextInt(59));
            calendar.set(Calendar.HOUR_OF_DAY, random.nextInt(isToday(operationDate) ? getHour(operationDate) : 23));
            calendar.set(Calendar.MILLISECOND, random.nextInt(999));
            return calendar.getTime();
        }
        return null;
    }

    /**
     * Checks if a date is today.
     *
     * @param date any
     * @return true if the day of the input is the same as today
     */
    public static boolean isToday(final Date date) {
        boolean isToday = false;
        if (date != null) {
            final Calendar today = Calendar.getInstance();
            final Calendar check = Calendar.getInstance();
            check.setTime(date);
            isToday |= (today.get(Calendar.ERA) == check.get(Calendar.ERA)) && (today.get(Calendar.YEAR) == check.get(Calendar.YEAR)) && (today.get(Calendar.DAY_OF_YEAR) == check.get(Calendar.DAY_OF_YEAR));
        }
        return isToday;
    }

    public static int getHour(final Date date) {
        int hour = 1;
        if (date != null) {
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            hour = calendar.get(Calendar.HOUR_OF_DAY);
        }
        return Math.min(Math.max(1, hour), 23);
    }

    public static int getDayOfMonth(final Date date) {
        int day = 1;
        if (date != null) {
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        }
        return day;
    }

    public static int getMonth(final Date date) {
        int month = 0;
        if (date != null) {
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            month = calendar.get(Calendar.MONTH);
        }
        return Math.min(Math.max(0, month), 11);
    }

    /**
     * Sets 00:00:00.000 as the time of the given date.
     *
     * @param operationDate any
     * @return any
     */
    public static Date getMidnight(final Date operationDate) {
        if (operationDate != null) {
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(operationDate);
            return getMidnight(calendar).getTime();
        }
        return null;
    }

    private static Calendar getMidnight(final Calendar calendar) {
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    /**
     * Adds some days to a date.
     *
     * @param date any
     * @param days any
     * @return any
     */
    public static Date add(final Date date, final int days) {
        Calendar calendar = null;
        if (date != null) {
            calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_MONTH, days);
        }
        return calendar.getTime();
    }

    public static int getMonths(final Date from, final Date to) {
        final Calendar calendar = Calendar.getInstance();
        final Calendar otherCalendar = Calendar.getInstance();
        calendar.setTime(from);
        otherCalendar.setTime(to);
        final int months = otherCalendar.get(Calendar.MONTH) - calendar.get(Calendar.MONTH);
        return months < 0 ? months + 12 : months;
    }

    public static String asQIF(final Date date) {
        final StringBuilder builder = new StringBuilder("D");
        if (date != null) {
            builder.append(formatter.format(date));
        }
        return builder.toString();
    }

    public static Date[] interval(Date origin, Date target) {
        final List<Date> dates = new ArrayList<Date>();
        if ((origin != null) && (target != null)) {
            final Calendar init = Calendar.getInstance();
            final Calendar finish = Calendar.getInstance();
            init.setTime(origin);
            init.add(Calendar.DAY_OF_YEAR, 1);
            finish.setTime(target);
            while (init.before(finish)) {
                dates.add(init.getTime());
                init.add(Calendar.DAY_OF_YEAR, 1);
            }
        }
        return dates.toArray(new Date[dates.size()]);
    }

    /**
     * Provides a Date in the past (at least many years ago).
     *
     * @return a valid date
     */
    public static Date getBeginningOfTime() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 1900);
        return calendar.getTime();
    }

}
