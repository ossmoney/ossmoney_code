package org.internna.ossmoney.util;

import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Utility methods for working with collections
 *
 * @author Jose Noheda
 * @since 1.0
 */
public final class CollectionUtils {

    private CollectionUtils() {
        throw new AssertionError("Do not try to instantiate an utility class");
    }

    public static boolean isEmpty(final Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotEmpty(final Collection<?> collection) {
        return !isEmpty(collection);
    }

    @SuppressWarnings("unchecked")
    public static <E, T extends Collection<E>> T add(final T collection, final E element) {
        T data = collection;
        if (element != null) {
            if (data == null) {
                data = (T) new ArrayList<E>();
            }
            data.add(element);
        }
        return data;
    }

    @SuppressWarnings("unchecked")
    public static <E, T extends Collection<E>> T addAll(final T collection, final T elements) {
        T data = collection;
        if (isNotEmpty(elements)) {
            if (data == null) {
                data = (T) new ArrayList<E>();
            }
            data.addAll(elements);
        }
        return data;
    }

    @SuppressWarnings("unchecked")
    public static <E, T extends Set<E>> T addAll(final T collection, final T elements) {
        T data = collection;
        if (isNotEmpty(elements)) {
            if (data == null) {
                data = (T) new HashSet<E>();
            }
            data.addAll(elements);
        }
        return data;
    }

    public static <T> Iterable<T> forEach(Iterable<T> iterable, Predicate<T> predicate) {
        if (iterable != null) {
            for (T element : iterable) {
                predicate.apply(element);
            }
        }
        return iterable;
    }

    public static interface Predicate<T> {
        void apply(T t);
    }

}
