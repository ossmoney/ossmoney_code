package org.internna.ossmoney.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;

public interface InvestmentPortfolioRepository extends JpaRepository<InvestmentPortfolio, String> {

	InvestmentPortfolio findByOwner(User owner);

}
