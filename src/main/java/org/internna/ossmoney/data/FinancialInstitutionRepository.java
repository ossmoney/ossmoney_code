package org.internna.ossmoney.data;

import java.util.List;

import org.internna.ossmoney.model.Country;
import org.internna.ossmoney.model.account.FinancialInstitution;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FinancialInstitutionRepository extends JpaRepository<FinancialInstitution, String> {

	FinancialInstitution findOne(String uuid);
	List<FinancialInstitution> findByCountry(Country country);

}