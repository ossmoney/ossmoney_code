package org.internna.ossmoney.data;

import java.util.List;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, String> {

	List<Account> findByOwnerAndVisible(User owner, Boolean visible);

}