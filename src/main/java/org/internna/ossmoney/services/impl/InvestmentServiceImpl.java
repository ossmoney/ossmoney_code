package org.internna.ossmoney.services.impl;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.math.BigDecimal;

import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.services.UserService;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.data.AccountRepository;
import org.internna.ossmoney.data.DepositRepository;
import org.internna.ossmoney.data.TransactionRepository;
import org.internna.ossmoney.services.InvestmentService;
import org.internna.ossmoney.model.transaction.Transaction;
import org.internna.ossmoney.model.investment.deposit.Deposit;
import org.internna.ossmoney.model.transaction.TransactionType;
import org.internna.ossmoney.data.InvestmentPortfolioRepository;
import org.internna.ossmoney.model.investment.deposit.DepositDTO;
import org.internna.ossmoney.data.FinancialInstitutionRepository;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;
import org.internna.ossmoney.model.investment.deposit.DepositTransaction;
import org.internna.ossmoney.model.investment.deposit.DepositTransactionType;

import static org.internna.ossmoney.util.StringUtils.hasText;

@Service
@Transactional
public final class InvestmentServiceImpl implements InvestmentService {

    private static final String INVALID_DEPOSIT = "Invalid deposit requested";

    private @Autowired UserService userService;
    private @Autowired AccountRepository accountRepository;
    private @Autowired DepositRepository depositRepository;
    private @Autowired TransactionRepository transactionRepository;
    private @Autowired InvestmentPortfolioRepository investmentRepository;
    private @Autowired FinancialInstitutionRepository financialInstitutionRepository;

    public @Override InvestmentPortfolio retrievePortfolio() {
        return investmentRepository.findByOwner(userService.currentUser());
    }

    public @Override Deposit save(final DepositDTO deposit) {
        Deposit saved = null;
        if (deposit != null) {
            final User user = userService.currentUser();
            final InvestmentPortfolio portfolio = investmentRepository.findByOwner(user);
            if (hasText(deposit.getUuid())) {
                saved = merge(deposit, portfolio);
            } else {
                saved = create(deposit, portfolio, user);
            }
            if (saved != null) {
                saved = depositRepository.save(saved);
                transferMoney(deposit, saved);
            }
        }
        return saved;
    }

    private Deposit create(final DepositDTO deposit, final InvestmentPortfolio portfolio, final User user) {
        final Deposit created = new Deposit();
        created.setPortfolio(portfolio);
        created.setName(deposit.getName());
        created.setOpened(deposit.getOpened());
        created.setDuration(deposit.getDuration());
        setTransaction(created, user, deposit.getBalance());
        created.setAnnualPercentageYield(deposit.getAnnualPercentageYield());
        created.setFinancialInstitution(financialInstitutionRepository.getOne(deposit.getFinancialInstitution()));
        return created;
    }

    private void setTransaction(final Deposit deposit, final User user, final BigDecimal value) {
        final MonetaryAmount amount = this.setTransaction(deposit, user, value, deposit.getOpened(), DepositTransactionType.CONTRACT);
        deposit.setBalance(amount);
    }

    private MonetaryAmount setTransaction(final Deposit deposit, final User user, final BigDecimal value, final Date date, final DepositTransactionType type) {
        final DepositTransaction transaction = new DepositTransaction();
        final List<DepositTransaction> transactions = hasText(deposit.getUuid()) ? deposit.getTransactions() : new ArrayList<>();
        transactions.add(transaction);
        deposit.setTransactions(transactions);
        final MonetaryAmount amount = type.equals(DepositTransactionType.WITHDRAWAL) ? deposit.getBalance() : Money.of(user.getCurrency(), value);
        transaction.setAmount(amount);
        transaction.setDeposit(deposit);
        transaction.setOperationDate(date);
        transaction.setTransactionType(type);
        return amount;
    }

    private void transferMoney(final DepositDTO deposit, final Deposit created) {
        if (hasText(deposit.getAccount())) {
            final Transaction transaction = new Transaction();
            final Account account = accountRepository.findOne(deposit.getAccount());
            transaction.setAccount(account);
            transaction.setMemo("account.deposit.transfer");
            transaction.setOperationDate(created.getOpened());
            transaction.setAmount(created.getBalance().negate());
            transaction.setTransactionType(TransactionType.TRANSFER);
            transactionRepository.save(transaction);
            account.setCurrentBalance(account.getCurrentBalance().add(transaction.getAmount()));
            accountRepository.save(account);
        }
    }

    public @Override Optional<Deposit> findDeposit(final String uuid) {
        Deposit loaded = hasText(uuid) ? depositRepository.findOne(uuid) : null;
        if (loaded != null) {
            final User user = userService.currentUser();
            if (!loaded.getPortfolio().getOwner().equals(user)) {
                throw new IllegalArgumentException(InvestmentServiceImpl.INVALID_DEPOSIT);
            }
        }
        return Optional.ofNullable(loaded);
    }

    private Deposit merge(final DepositDTO deposit, final InvestmentPortfolio portfolio) {
        Deposit merge = null;
        final Optional<Deposit> loaded = findDeposit(deposit.getUuid());
        if (loaded.isPresent()) {
            merge = loaded.get();
            merge.setName(deposit.getName());
            merge.setOpened(deposit.getOpened());
            merge.setDuration(deposit.getDuration());
            merge.setAnnualPercentageYield(deposit.getAnnualPercentageYield());
            if (merge.getBalance().getNumber().doubleValueExact() != deposit.getBalance().doubleValue()) {
                merge.setBalance(Money.of(merge.getBalance().getCurrency(), deposit.getBalance()));
                final DepositTransaction buy = merge.getTransactions().get(0);
                buy.setAmount(merge.getBalance());
                buy.setOperationDate(merge.getOpened());
            }
        }
        return merge;
    }

    public @Override Optional<Deposit> cancel(final DepositDTO dto) {
        Optional<Deposit> loaded = findDeposit(dto.getUuid());
        if (loaded.isPresent()) {
            Deposit deposit = loaded.get();
            boolean totalCancellation = dto.getBalance().abs().compareTo(deposit.getBalance().getNumber().numberValue(BigDecimal.class)) >= 0;
            final MonetaryAmount amount = setTransaction(deposit, userService.currentUser(), dto.getBalance(), dto.getOpened(), totalCancellation ? DepositTransactionType.WITHDRAWAL : DepositTransactionType.PARTIAL_CANCELLATION);
            deposit.setBalance(deposit.getBalance().add(amount));
            if (totalCancellation) {
                deposit.setVisible(false);
            }
            deposit = depositRepository.save(deposit);
            loaded = Optional.ofNullable(deposit.isVisible() ? deposit : null);
        }
        return loaded;
    }
}
