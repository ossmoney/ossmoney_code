package org.internna.ossmoney.services;

import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.model.security.UserDTO;

public interface UserService {

    User currentUser();

    User update(UserDTO user);

}
