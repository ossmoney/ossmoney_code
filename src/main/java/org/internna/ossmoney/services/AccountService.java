package org.internna.ossmoney.services;

import java.util.List;
import java.util.Optional;
import java.io.IOException;
import java.math.BigDecimal;
import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.model.account.FinancialInstitution;

public interface AccountService {

    Optional<Account> find(String uuid);

    Optional<Account> close(String uuid);

    Optional<Account> reopen(String uuid);

    List<Account> findAccounts();

    byte[] export() throws IOException;

    Optional<Account> save(Account account, BigDecimal balance);

    Optional<FinancialInstitution> save(FinancialInstitution bank);

    List<FinancialInstitution> findFinancialInstitutions(String country);

}
