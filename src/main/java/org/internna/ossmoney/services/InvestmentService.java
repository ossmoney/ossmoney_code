package org.internna.ossmoney.services;

import java.util.Optional;
import org.internna.ossmoney.model.investment.deposit.Deposit;
import org.internna.ossmoney.model.investment.deposit.DepositDTO;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;

public interface InvestmentService {

    Deposit save(DepositDTO deposit);

    InvestmentPortfolio retrievePortfolio();

    Optional<Deposit> findDeposit(String uuid);

    Optional<Deposit> cancel(DepositDTO deposit);

}
