package org.internna.ossmoney.controllers.js;

import javax.servlet.ServletContext;
import net.jawr.web.servlet.JawrSpringController;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractJawrController extends JawrSpringController {

	private static final String JAWR_PROPETIES_LOCATION = "jawr.properties";

	@Autowired private ServletContext servletContext;

	public AbstractJawrController(final String type) {
		setType(type);
		setConfigLocation(AbstractJawrController.JAWR_PROPETIES_LOCATION);
	}

	@Override public final void afterPropertiesSet() throws Exception {
		setServletContext(servletContext);
		super.afterPropertiesSet();
	}

}
