package org.internna.ossmoney.controllers.js;

import javax.servlet.http.HttpServletRequest;
import org.webjars.WebJarAssetLocator;
import org.springframework.http.HttpStatus;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(WebjarsController.WEBJARS_LOCATOR_PATH)
public final class WebjarsController {

	public static final String WEBJARS_LOCATOR_PATH = "/webjarslocator";

	private final WebJarAssetLocator assetLocator = new WebJarAssetLocator();

	@ResponseBody
	@RequestMapping("/{webjar}/**")
	public ResponseEntity<Resource> locateWebjarAsset(@PathVariable final String webjar, final HttpServletRequest request) {
		try {
			final String mvcPrefix = WebjarsController.WEBJARS_LOCATOR_PATH + "/" + webjar + "/";
			final String mvcPath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
			final String fullPath = assetLocator.getFullPath(webjar, mvcPath.substring(mvcPrefix.length()));
			return new ResponseEntity<Resource>(new ClassPathResource(fullPath), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
