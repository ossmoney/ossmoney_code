package org.internna.ossmoney.controllers.js;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import static net.jawr.web.JawrConstant.CSS_TYPE;

@Controller
public final class JawrCssController extends AbstractJawrController {

	public JawrCssController() {
		super(CSS_TYPE);
	}

	@RequestMapping(value = "/**/**.css", method = RequestMethod.GET)
	@Override public ModelAndView handleRequest(final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		return super.handleRequest(request, response);
	}

}
