package org.internna.ossmoney.controllers;

import java.util.List;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.internna.ossmoney.model.security.User;
import org.internna.ossmoney.services.UserService;
import org.internna.ossmoney.model.security.UserDTO;
import org.internna.ossmoney.services.AccountService;
import org.internna.ossmoney.model.account.FinancialInstitution;

import static org.internna.ossmoney.model.security.User.DEMO_USER;

@Controller
public final class MainController {

    private @Autowired UserService userService;
    private @Autowired SignInAdapter singInAdapter;
    private @Autowired AccountService accountService;
    private @Value("${activate.demo:false}") boolean activateDemo;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginPage(final Model model) {
        model.addAttribute("demo", activateDemo);
        return "security/login";
    }

    @RequestMapping(value = "/demo", method = RequestMethod.GET)
    public String loginDemoUser() {
        singInAdapter.signIn(DEMO_USER, null, null);
        return showMainPage();
    }

    @ResponseBody
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public User user() {
        return userService.currentUser();
    }

    @ResponseBody
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public User update(@RequestBody final UserDTO user) {
        return userService.update(user);
    }

    @ResponseBody
    @RequestMapping(value = "/banks/{country:.+}", method = RequestMethod.GET)
    public List<FinancialInstitution> financialInstitutions(@PathVariable("country") final String country) {
        return accountService.findFinancialInstitutions(country);
    }

    @RequestMapping(value = "/fonts/{font:.+}", method = RequestMethod.GET)
    public String redirectFonts(@PathVariable final String font) {
        return "redirect:" + (font.contains("fontawesome") ? "/webjarslocator/font-awesome/fonts/" : font.contains("glyphicon") ? "/webjarslocator/bootstrap/dist/fonts/" : "/webfonts/") + font;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showMainPage() {
        return "index";
    }

}
