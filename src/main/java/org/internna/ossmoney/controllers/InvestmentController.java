package org.internna.ossmoney.controllers;

import java.util.Optional;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.internna.ossmoney.services.InvestmentService;
import org.internna.ossmoney.model.investment.deposit.Deposit;
import org.internna.ossmoney.model.investment.deposit.DepositDTO;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;

@Controller
@RequestMapping(value = "/investment")
public final class InvestmentController {

    private @Autowired InvestmentService investmentService;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody InvestmentPortfolio retrievePortfolio() {
        return investmentService.retrievePortfolio();
    }

    @RequestMapping(value = "deposit", method = RequestMethod.POST)
    public @ResponseBody Deposit save(@RequestBody final DepositDTO deposit) {
        return investmentService.save(deposit);
    }

    @RequestMapping(value = "deposit/cancel", method = RequestMethod.POST)
    public @ResponseBody Optional<Deposit> cancel(@RequestBody final DepositDTO deposit) {
        return investmentService.cancel(deposit);
    }

}
