package org.internna.ossmoney.controllers;

import java.util.List;
import java.util.Optional;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.internna.ossmoney.model.account.Account;
import org.internna.ossmoney.services.AccountService;
import org.internna.ossmoney.model.account.AccountDTO;
import org.internna.ossmoney.model.account.AccountType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/account")
public final class AccountController {

    private @Autowired AccountService accountService;

    @ResponseBody
    @RequestMapping(value = "/type", method = RequestMethod.GET)
    public AccountType[] obtainAccountTpes() {
        return AccountType.values();
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public List<Account> obtainAccounts() {
        return accountService.findAccounts();
    }

    @ResponseBody
    @RequestMapping(value = "/{uuid}", method = RequestMethod.DELETE)
    public Optional<Account> delete(@PathVariable("uuid") final String uuid) {
        return accountService.close(uuid);
    }

    @RequestMapping("/export")
    public @ResponseBody byte[] backup(final HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment; filename=\"ossmoney-accounts-backup.zip\"");
        return accountService.export();
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public Optional<Account> create(@RequestBody final AccountDTO account) {
        return accountService.save(account.getAccount(), account.getInitialBalance());
    }

    @ResponseBody
    @RequestMapping(value = "/reopen", method = RequestMethod.POST)
    public Optional<Account> reopen(@RequestBody final AccountDTO account) {
        return accountService.reopen(account.getUuid());
    }

}
