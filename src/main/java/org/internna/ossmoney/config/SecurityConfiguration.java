package org.internna.ossmoney.config;

import org.internna.ossmoney.model.security.CombinedUser;
import org.internna.ossmoney.controllers.js.WebjarsController;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.security.SpringSocialConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

@Configuration
@EnableWebMvcSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String LOGIN_PATH = "/login";

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.formLogin().loginPage(SecurityConfiguration.LOGIN_PATH).failureUrl(SecurityConfiguration.LOGIN_PATH + "?error=1").and().logout().logoutSuccessUrl("/").logoutRequestMatcher(new AntPathRequestMatcher("/logout")).and().authorizeRequests()
                .antMatchers("/js/**", "/css/**", "/auth/**", "/demo/**", "/banks/**", "/images/**", "/angular/**", "/webjars/**", "/favicon.ico", "/gzip*/js/**", "/gzip*/css/**", "/account/type", "/jawr_generator.js", "/jawr_generator.css", SecurityConfiguration.LOGIN_PATH + "/**", WebjarsController.WEBJARS_LOCATOR_PATH + "/**").permitAll().antMatchers("/admin/**").hasRole(CombinedUser.ADMIN_ROLE).antMatchers("/**").authenticated().and().apply(getSocialConfigurer());
    }

    private SpringSocialConfigurer getSocialConfigurer() {
        return new SpringSocialConfigurer().postLoginUrl("/").alwaysUsePostLoginUrl(true);
    }

}
