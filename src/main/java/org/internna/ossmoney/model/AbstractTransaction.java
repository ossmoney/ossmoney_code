package org.internna.ossmoney.model;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Columns;
import java.util.Date;
import javax.money.MonetaryAmount;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

@MappedSuperclass
public abstract class AbstractTransaction extends AbstractEntity {

	static final String MONEY_TYPE = "MONEY_TYPE";

	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "S-")
	@Column(name = "OPERATION_DATE", nullable = false)
	private Date operationDate;

	@NotNull
	@Type(type = AbstractTransaction.MONEY_TYPE)
	@Columns(columns = {
		@Column(name = "CURRENCY", nullable = false),
		@Column(name = "AMOUNT", nullable = false)
	})
	private MonetaryAmount amount;

	public final Date getOperationDate() {
		return operationDate;
	}

	public final void setOperationDate(final Date operationDate) {
		this.operationDate = operationDate;
	}

	public final MonetaryAmount getAmount() {
		return amount;
	}

	public final void setAmount(final MonetaryAmount amount) {
		this.amount = amount;
	}

}
