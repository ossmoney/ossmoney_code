package org.internna.ossmoney.model.transaction;

public enum TransactionType {

    INCOME, EXPENSE, TRANSFER;

}
