package org.internna.ossmoney.model.transaction;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;
import java.util.Collection;
import org.javamoney.moneta.Money;
import org.internna.ossmoney.model.account.Account;

import static org.internna.ossmoney.util.StringUtils.hasText;

public final class TransactionDTO {

    private String[] tags;
    private BigDecimal amount;
    private Date operationDate;
    private TransactionType transactionType;
    private String uuid, account, memo, referenceNumber, chargeAccount;

    public String getAccount() {
        return account;
    }

    public void setAccount(final String account) {
        this.account = account;
    }

    public void setTags(final String[] tags) {
        this.tags = tags;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(final Date operationDate) {
        this.operationDate = operationDate;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(final TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(final String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

    public Collection<Tags> getTags() {
        boolean transfer = false;
        final List<Tags> categories = new ArrayList<Tags>();
        if ((tags != null) && (tags.length > 0)) {
            for (String tag : tags) {
                if (hasText(tag)) {
                    final Tags found = Tags.valueOf(tag);
                    categories.add(found);
                    transfer |= Tags.TRANSFER.equals(found);
                }
            }
        }
        if (TransactionType.TRANSFER.equals(this.transactionType) & !transfer) {
            categories.add(Tags.TRANSFER);
        }
        return categories;
    }

    public Transaction asTransaction(final Account ccc) {
        final Transaction transaction = new Transaction();
        transaction.setAccount(ccc);
        transaction.setMemo(this.memo);
        transaction.setTags(getTags());
        transaction.setOperationDate(this.operationDate);
        transaction.setReferenceNumber(this.referenceNumber);
        transaction.setTransactionType(this.transactionType);
        transaction.setUuid(hasText(this.uuid) ? this.uuid : null);
        transaction.setAmount(Money.of(ccc.getInitialBalance().getCurrency(), this.amount));
        return transaction;
    }

}
