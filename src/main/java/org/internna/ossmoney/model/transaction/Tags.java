package org.internna.ossmoney.model.transaction;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.internna.ossmoney.model.transaction.Tags.TagsSerializer;

@JsonSerialize(using = TagsSerializer.class)
public enum Tags {

    CAR_BUY(TagCategory.AUTOMOBILE), GASOLINE(TagCategory.AUTOMOBILE), CAR_RENT(TagCategory.AUTOMOBILE), CAR_INSURANCE(TagCategory.AUTOMOBILE, TagCategory.INSURANCE), CAR_LOAN_PAYMENT(TagCategory.AUTOMOBILE, TagCategory.FINANCIAL), CAR_MAINTENANCE("fa fa-wrench", TagCategory.AUTOMOBILE),

    TOYS("el el-puzzle", TagCategory.CHILDCARE), BABYSITTING(TagCategory.CHILDCARE), CHILD_SUPPORT(TagCategory.CHILDCARE),

    TUITION(TagCategory.EDUCATION), MATERIAL(TagCategory.EDUCATION), STUDENT_LOAN_PAYMENT(TagCategory.EDUCATION, TagCategory.FINANCIAL),

    REIMBURSEMENT(TagCategory.FINANCIAL), BANK_CHARGE("fa fa-university", TagCategory.FINANCIAL), CREDIT_CARD_FEES("fa fa-credit-card", TagCategory.FINANCIAL), FINANCE_CHARGE(TagCategory.FINANCIAL), INTEREST("glyphicon glyphicon-piggy-bank", TagCategory.FINANCIAL), LOAN(TagCategory.FINANCIAL),

    COFFEE("fa fa-coffee", TagCategory.FOOD), GROCERIES("glyphicon glyphicon-scale", TagCategory.FOOD), DINING_OUT("fa fa-cutlery", TagCategory.FOOD, TagCategory.LEISURE), VENDING_MACHINE(TagCategory.FOOD),

    DENTAL_CARE(TagCategory.HEALTHCARE), VISION("el el-glasses", TagCategory.HEALTHCARE), PHYSICIAN("fa fa-user-md", TagCategory.HEALTHCARE), HOSPITAL("fa fa-h-square", TagCategory.HEALTHCARE), PRESCRIPTIONS("fa fa-medkit", TagCategory.HEALTHCARE),

    ASSOCIATION_DUES("fa fa-users", TagCategory.HOUSEHOLD), FURNITURES(TagCategory.HOUSEHOLD), HOME_MAINTENANCE("el el-broom", TagCategory.HOUSEHOLD), HOME_TAX(TagCategory.HOUSEHOLD, TagCategory.TAXES), RENT(TagCategory.HOUSEHOLD),

    LIFE_INSURANCE(TagCategory.INSURANCE),

    MAGAZINES("fa fa-book", TagCategory.LEISURE), BEERS("fa fa-beer", TagCategory.LEISURE), CINEMA("el el-film", TagCategory.LEISURE), MUSIC("fa fa-music", TagCategory.LEISURE), EVENTS("fa fa-futbol-o", TagCategory.LEISURE), VOD("fa fa-play", TagCategory.LEISURE), VIDEOGAMES("fa fa-gamepad", TagCategory.LEISURE),

    BONUS("fa fa-certificate", TagCategory.JOB), PAYCHECK(TagCategory.JOB), TRAVEL("fa fa-suitcase", TagCategory.JOB),

    LOTTERY(TagCategory.MISC), GAMBLING(TagCategory.MISC), CASH_ATM("fa fa-money", TagCategory.MISC), CHARITY(TagCategory.MISC), GIFTS("fa fa-gift", TagCategory.MISC), TRANSFER("glyphicon glyphicon-retweet", TagCategory.MISC),

    PET_FOOD(TagCategory.PETS), VETERINARIAN(TagCategory.PETS),

    CLOTHING(TagCategory.SHOPPING), ELECTRONICS("fa fa-tablet", TagCategory.SHOPPING),

    GOVERMENT_TAX(TagCategory.TAXES),

    WATER("fa fa-tint", TagCategory.UTILITIES), SEWER(TagCategory.UTILITIES), ELECTRICITY("fa fa-bolt", TagCategory.UTILITIES), GAS("glyphicon glyphicon-fire", TagCategory.UTILITIES), INTERNET("fa fa-wifi", TagCategory.UTILITIES), TELEPHONE("glyphicon glyphicon-phone-alt", TagCategory.UTILITIES), GARBAGE("glyphicon glyphicon-trash", TagCategory.UTILITIES),

    TOURISM("glyphicon glyphicon-camera", TagCategory.VACATION), TRANSPORTATION("fa fa-plane", TagCategory.VACATION), LODGING("fa fa-bed", TagCategory.VACATION), LUGGAGE("glyphicon glyphicon-briefcase", TagCategory.VACATION);

    private final String icon;
    private final TagCategory[] categories;

    private Tags(final TagCategory... categories) {
        this(null, categories);
    }

    private Tags(final String icon, final TagCategory... categories) {
        this.icon = icon;
        this.categories = categories;
    }

    public String getIcon() {
        return icon;
    }

    public enum TagCategory {
        AUTOMOBILE("fa fa-car"), CHILDCARE("fa fa-child"), EDUCATION("fa fa-graduation-cap"), FINANCIAL("fa fa-money"), FOOD("glyphicon glyphicon-grain"), HEALTHCARE("fa fa-heartbeat"), HOUSEHOLD("fa fa-home"), INSURANCE("fa fa-life-ring"), LEISURE("fa fa-glass"), JOB("fa fa-building-o"), MISC("fa fa-asterisk"), PETS("el el-guidedog"), SHOPPING("fa fa-shopping-cart"), TAXES("fa fa-gavel"), UTILITIES("fa fa-cogs"), VACATION("fa fa-globe");

        private final String icon;

        private TagCategory(final String icon) {
            this.icon = icon;
        }

        public String getIcon() {
            return icon;
        }

    }

    public TagCategory[] getCategories() {
        return this.categories;
    }

    public static class TagsSerializer extends JsonSerializer<Tags> {

        @Override
        public void serialize(Tags tag, JsonGenerator generator, SerializerProvider provider) throws IOException, JsonProcessingException {
            generator.writeStartObject();
            generator.writeFieldName("tag");
            generator.writeString(tag.name());
            generator.writeFieldName("icon");
            generator.writeString(tag.getIcon());
            generator.writeArrayFieldStart("categories");
            final TagCategory[] categories = tag.getCategories();
            for (TagCategory category : categories) {
                generator.writeStartObject();
                generator.writeFieldName("tag");
                generator.writeString(category.name());
                generator.writeFieldName("icon");
                generator.writeString(category.getIcon());
                generator.writeEndObject();
            }
            generator.writeEndArray();
            generator.writeEndObject();
        }

    }

}
