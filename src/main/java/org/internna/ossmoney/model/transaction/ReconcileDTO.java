package org.internna.ossmoney.model.transaction;

import java.util.List;

public class ReconcileDTO {

    private List<String> transactions;
    private String account, chargeAccount;

    public String getAccount() {
        return account;
    }

    public void setAccount(final String account) {
        this.account = account;
    }

    public List<String> getTransactions() {
        return transactions;
    }

    public void setTransactions(final List<String> transactions) {
        this.transactions = transactions;
    }

    public String getChargeAccount() {
        return chargeAccount;
    }

    public void setChargeAccount(final String chargeAccount) {
        this.chargeAccount = chargeAccount;
    }

}
