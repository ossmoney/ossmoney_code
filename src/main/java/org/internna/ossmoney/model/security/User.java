package org.internna.ossmoney.model.security;

import java.util.Date;
import java.util.Locale;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.money.CurrencyUnit;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.PrePersist;
import javax.money.MonetaryCurrencies;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import static org.internna.ossmoney.util.StringUtils.hasText;

@Entity
@Table(name = User.USERS)
public class User {

    public static final String USERS = "USERS";
    public static final String DEMO_USER = "demo";
    public static final String DEFAULT_SKIN = "brown";
    public static final Locale DEFAULT_LOCALE = new Locale.Builder().setLanguage("es").setRegion("ES").build();

    @Id
    @Column(name = "USERNAME", nullable = false)
    private String username;

    @NotNull
    @DateTimeFormat(style = "S-")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED", nullable = false)
    private Date created;

    @NotNull
    @Column(name = "SKIN", nullable = false)
    private String skin;

    @NotNull
    @Column(name = "LOCALE", nullable = false)
    @Type(type = "org.hibernate.type.LocaleType")
    private Locale locale;

    @NotNull
    @Column(name = "CURRENCY", nullable = false)
    @Type(type = "org.jadira.usertype.moneyandcurrency.moneta.PersistentCurrencyUnit")
    private CurrencyUnit currency;

    @NotNull
    @Column(name = "ADMINISTRATOR", nullable = false)
    private boolean administrator;

    public @Override boolean equals(final Object obj) {
        return obj instanceof User ? hasText(username) ? username.equals(((User) obj).username) : false : false;
    }

    public @Override int hashCode() {
        return hasText(username) ? username.hashCode() + 1234 : super.hashCode();
    }

    @PrePersist
    public void fill() {
        if (!hasText(getSkin())) {
            setSkin(User.DEFAULT_SKIN);
        }
        if (getLocale() == null) {
            setLocale(User.DEFAULT_LOCALE);
        }
        if (getCurrency() == null) {
            setCurrency(MonetaryCurrencies.getCurrency(getLocale()));
        }
        setCreated(new Date());
    }

    public final String getUsername() {
        return username;
    }

    public final void setUsername(final String username) {
        this.username = username;
    }

    public final Date getCreated() {
        return created;
    }

    public final void setCreated(final Date created) {
        this.created = created;
    }

    public final Locale getLocale() {
        return locale;
    }

    public final void setLocale(final Locale locale) {
        this.locale = locale;
    }

    public final String getSkin() {
        return skin;
    }

    public final void setSkin(final String skin) {
        this.skin = skin;
    }

    public final CurrencyUnit getCurrency() {
        return currency;
    }

    public final void setCurrency(final CurrencyUnit currency) {
        this.currency = currency;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(final boolean administrator) {
        this.administrator = administrator;
    }

    public final static class UserBuilder {

        private final String username;

        private String skin;
        private Locale locale;
        private CurrencyUnit currency;
        private boolean administrator = false;

        public UserBuilder(final String username) {
            this.username = username;
        }

        public UserBuilder addSkin(final String skin) {
            this.skin = skin;
            return this;
        }

        public UserBuilder addLocale(final Locale locale) {
            this.locale = locale;
            if ((this.locale != null) & (this.currency == null)) {
                addCurrency(MonetaryCurrencies.getCurrency(locale));
            }
            return this;
        }

        public UserBuilder addCurrency(final CurrencyUnit currency) {
            this.currency = currency;
            return this;
        }

        public UserBuilder administrator(final boolean administrator) {
            this.administrator = administrator;
            return this;
        }

        public User build() {
            final User user = new User();
            user.setSkin(this.skin);
            user.setLocale(this.locale);
            user.setCurrency(this.currency);
            user.setUsername(this.username);
            user.setAdministrator(this.administrator);
            return user;
        }

    }

}
