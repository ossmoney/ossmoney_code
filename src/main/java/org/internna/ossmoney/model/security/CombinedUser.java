package org.internna.ossmoney.model.security;

import java.util.List;
import java.util.ArrayList;
import org.springframework.social.security.SocialUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public final class CombinedUser extends SocialUser {

    public static final String USER_ROLE = "USER";
    public static final String ADMIN_ROLE = "ADMIN";

    private static final String ROLE = "ROLE_";
    private static final long serialVersionUID = -7055798374511447499L;
    private static final String UNAVAILABLE_PASSWORD = "<NOT_AVAILABLE>";

    private final User user;

    public CombinedUser(final User user) {
        super(user.getUsername(), CombinedUser.UNAVAILABLE_PASSWORD, CombinedUser.getDefaultAuthorities(user));
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public static List<GrantedAuthority> getDefaultAuthorities(final User user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(CombinedUser.ROLE + CombinedUser.USER_ROLE));
        if (user.isAdministrator()) {
            authorities.add(new SimpleGrantedAuthority(CombinedUser.ROLE + CombinedUser.ADMIN_ROLE));
        }
        return authorities;
    }

}
