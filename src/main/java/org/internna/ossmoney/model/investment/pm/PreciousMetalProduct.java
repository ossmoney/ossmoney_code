package org.internna.ossmoney.model.investment.pm;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import org.internna.ossmoney.model.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = PreciousMetalProduct.PRECIOUS_METAL_PRODUCT)
public class PreciousMetalProduct extends AbstractEntity {

	static final String MONEY_TYPE = "MONEY_TYPE";
	public static final String PRECIOUS_METAL_PRODUCT = "PRECIOUS_METAL_PRODUCT"; 

	public enum PreciousMetalShape {
		COIN, BAR, NUGGET;
	}

	public enum PreciousMetalMint {
		US_MINT, ROYAL_CANADIAN_MINT, UK_MINT, PERTH_MINT, CHINA_MINT, UNKOWN_MINT;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "SHAPE", nullable = false)
	private PreciousMetalShape shape;

	@NotNull
	@JsonIgnore
	@Enumerated(EnumType.STRING)
	@Column(name = "METAL", nullable = false)
	private PreciousMetalType preciousMetalType;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "MINT", nullable = false)
	private PreciousMetalMint mint;

	@NotNull
	@Column(name = "NAME", nullable = false)
	private String name;

	@NotNull
	@Column(name = "WEIGHT", nullable = false, columnDefinition = "DECIMAL")
	private float weight;

	@NotNull
	@Column(name = "WEIGHT_METAL", nullable = false, columnDefinition = "DECIMAL")
	private float weightInMetal;

	@NotNull
	@Column(name = "PURITY", nullable = false, columnDefinition = "DECIMAL")
	private float purity;

	public final PreciousMetalShape getShape() {
		return shape;
	}

	public final void setShape(final PreciousMetalShape shape) {
		this.shape = shape;
	}

	public final PreciousMetalType getPreciousMetalType() {
		return preciousMetalType;
	}

	public final void setPreciousMetalType(final PreciousMetalType preciousMetalType) {
		this.preciousMetalType = preciousMetalType;
	}

	public final String getName() {
		return name;
	}

	public final void setName(final String name) {
		this.name = name;
	}

	public final float getWeight() {
		return weight;
	}

	public final void setWeight(final float weight) {
		this.weight = weight;
	}

	public final float getWeightInMetal() {
		return weightInMetal;
	}

	public final void setWeightInMetal(final float weightInMetal) {
		this.weightInMetal = weightInMetal;
	}

	public final PreciousMetalMint getMint() {
		return mint;
	}

	public final void setMint(final PreciousMetalMint mint) {
		this.mint = mint;
	}

	public final float getPurity() {
		return purity;
	}

	public final void setPurity(final float purity) {
		this.purity = purity;
	}

}
