package org.internna.ossmoney.model.investment.pm;

import java.util.Date;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.money.MonetaryAmount;
import javax.persistence.Temporal;
import javax.persistence.Enumerated;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Columns;
import org.internna.ossmoney.model.AbstractEntity;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = PreciousMetalPrice.PRECIOUS_METAL_PRICE)
public class PreciousMetalPrice extends AbstractEntity {

	static final String MONEY_TYPE = "MONEY_TYPE";
	public static final String PRECIOUS_METAL_PRICE = "PRECIOUS_METAL_PRICE"; 

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "METAL", nullable = false)
	private PreciousMetalType preciousMetalType;

	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "S-")
	@Column(name = "OPERATION_DATE", nullable = false)
	private Date date;

	@NotNull
	@Type(type = PreciousMetalPrice.MONEY_TYPE)
	@Columns(columns = {
		@Column(name = "SPOT_CURRENCY", nullable = false),
		@Column(name = "SPOT_AMOUNT", nullable = false)
	})
	private MonetaryAmount spot;

	@NotNull
	@Type(type = PreciousMetalPrice.MONEY_TYPE)
	@Columns(columns = {
		@Column(name = "SELL_CURRENCY", nullable = false),
		@Column(name = "SELL_AMOUNT", nullable = false)
	})
	private MonetaryAmount sell;

	public final PreciousMetalType getPreciousMetalType() {
		return preciousMetalType;
	}

	public final void setPreciousMetalType(final PreciousMetalType preciousMetalType) {
		this.preciousMetalType = preciousMetalType;
	}

	public final Date getDate() {
		return date;
	}

	public final void setDate(final Date date) {
		this.date = date;
	}

	public final MonetaryAmount getSpot() {
		return spot;
	}

	public final void setSpot(final MonetaryAmount spot) {
		this.spot = spot;
	}

	public final MonetaryAmount getSell() {
		return sell;
	}

	public final void setSell(final MonetaryAmount sell) {
		this.sell = sell;
	}

}
