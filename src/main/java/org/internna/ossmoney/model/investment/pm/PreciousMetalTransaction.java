package org.internna.ossmoney.model.investment.pm;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.ManyToOne;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.internna.ossmoney.model.AbstractTransaction;

@Entity
@Table(name = PreciousMetalTransaction.PRECIOUS_METAL_TRANSACTION)
public class PreciousMetalTransaction extends AbstractTransaction {

	public static final String PRECIOUS_METAL_TRANSACTION = "PRECIOUS_METAL_TRANSACTION";

	@NotNull
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "PRECIOUS_METAL", nullable = false, updatable = false)
	private PreciousMetal preciousMetal;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "TRANSACTION_TYPE", nullable = false)
	private PreciousMetalTransactionType transactionType;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "PRODUCT", nullable = false, updatable = false)
	private PreciousMetalProduct product;

	public final PreciousMetal getPreciousMetal() {
		return preciousMetal;
	}

	public final void setPreciousMetal(final PreciousMetal preciousMetal) {
		this.preciousMetal = preciousMetal;
	}

	public final PreciousMetalTransactionType getTransactionType() {
		return transactionType;
	}

	public final void setTransactionType(final PreciousMetalTransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public final PreciousMetalProduct getProduct() {
		return product;
	}

	public final void setProduct(final PreciousMetalProduct product) {
		this.product = product;
	}

}
