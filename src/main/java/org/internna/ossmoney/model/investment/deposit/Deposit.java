package org.internna.ossmoney.model.investment.deposit;

import java.util.Date;
import java.util.List;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.money.MonetaryAmount;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.internna.ossmoney.model.AbstractEntity;
import org.internna.ossmoney.model.account.FinancialInstitution;
import org.internna.ossmoney.model.investment.InvestmentPortfolio;
import org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency;

@Entity
@Table(name = Deposit.DEPOSIT)
@TypeDef(name = Deposit.MONEY_TYPE, typeClass = PersistentMoneyAmountAndCurrency.class)
public class Deposit extends AbstractEntity {

    static final String MONEY_TYPE = "MONEY_TYPE";
    public static final String DEPOSIT = "DEPOSIT";

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @Column(name = "ANUAL_YIELD", nullable = false, columnDefinition = "DECIMAL")
    private float annualPercentageYield;

    @NotNull
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "S-")
    @Column(name = "OPENED", nullable = false)
    private Date opened;

    @NotNull
    @Column(name = "DURATION", nullable = false)
    private int duration;

    @NotNull
    @Type(type = Deposit.MONEY_TYPE)
    @Columns(columns = {@Column(name = "BALANCE_CURRENCY", nullable = false), @Column(name = "BALANCE_AMOUNT", nullable = false)})
    private MonetaryAmount balance;

    @Fetch(FetchMode.SELECT)
    @OrderBy("operationDate ASC")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "deposit")
    @Filter(name = AbstractEntity.VISIBLE_ENTITY, condition = "visible = :" + AbstractEntity.VISIBLE_PARAM)
    private List<DepositTransaction> transactions;

    @NotNull
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PORTFOLIO", nullable = false, updatable = false)
    private InvestmentPortfolio portfolio;

    @NotNull
    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FINANCIAL_INSTITUTION", nullable = false)
    private FinancialInstitution financialInstitution;

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final float getAnnualPercentageYield() {
        return annualPercentageYield;
    }

    public final void setAnnualPercentageYield(final float annualPercentageYield) {
        this.annualPercentageYield = annualPercentageYield;
    }

    public final int getDuration() {
        return duration;
    }

    public final void setDuration(final int duration) {
        this.duration = duration;
    }

    public final MonetaryAmount getBalance() {
        return balance;
    }

    public final void setBalance(final MonetaryAmount balance) {
        this.balance = balance;
    }

    public final List<DepositTransaction> getTransactions() {
        return transactions;
    }

    public final void setTransactions(final List<DepositTransaction> transactions) {
        this.transactions = transactions;
    }

    public final InvestmentPortfolio getPortfolio() {
        return portfolio;
    }

    public final void setPortfolio(final InvestmentPortfolio portfolio) {
        this.portfolio = portfolio;
    }

    public final FinancialInstitution getFinancialInstitution() {
        return financialInstitution;
    }

    public final void setFinancialInstitution(final FinancialInstitution financialInstitution) {
        this.financialInstitution = financialInstitution;
    }

    public Date getOpened() {
        return opened;
    }

    public void setOpened(final Date opened) {
        this.opened = opened;
    }

}
