package org.internna.ossmoney.model.investment.deposit;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.ManyToOne;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.internna.ossmoney.model.AbstractTransaction;

@Entity
@Table(name = DepositTransaction.DEPOSIT_TRANSACTION)
public class DepositTransaction extends AbstractTransaction {

	public static final String DEPOSIT_TRANSACTION = "DEPOSIT_TRANSACTION";

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "TRANSACTION_TYPE", nullable = false)
	private DepositTransactionType transactionType;

	@NotNull
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "DEPOSIT", nullable = false, updatable = false)
	private Deposit deposit;

	public final Deposit getDeposit() {
		return deposit;
	}

	public final void setDeposit(final Deposit deposit) {
		this.deposit = deposit;
	}

	public final DepositTransactionType getTransactionType() {
		return transactionType;
	}

	public final void setTransactionType(final DepositTransactionType transactionType) {
		this.transactionType = transactionType;
	}

}
