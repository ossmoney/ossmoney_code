package org.internna.ossmoney.model.investment.pm;

public enum PreciousMetalType {

	GOLD, SILVER, PALLADIUM, PLATINUM;

}
