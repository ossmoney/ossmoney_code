package org.internna.ossmoney.model;

import java.util.Date;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Version;
import javax.persistence.Temporal;
import javax.persistence.PreUpdate;
import javax.persistence.PrePersist;
import javax.persistence.TemporalType;
import javax.persistence.GeneratedValue;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import static org.internna.ossmoney.util.StringUtils.hasText;

@MappedSuperclass
@FilterDef(name = AbstractEntity.VISIBLE_ENTITY, parameters = @ParamDef(name = AbstractEntity.VISIBLE_PARAM, type = "boolean"))
public abstract class AbstractEntity {

    public static final String VISIBLE_PARAM = "visible";
    public static final String VISIBLE_ENTITY = "visibleEntity";

    @Id
    @Column(name = "UUID", nullable = false)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String uuid;

    @Version
    @JsonIgnore
    @Column(name = "VERSION")
    private Integer version;

    @NotNull
    @JsonIgnore
    @DateTimeFormat(style = "S-")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED", nullable = false)
    private Date created;

    @NotNull
    @JsonIgnore
    @DateTimeFormat(style = "S-")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATE", nullable = false)
    private Date lastUpdate;

    @NotNull
    @JsonIgnore
    @Column(name = "VISIBLE", nullable = false)
    private boolean visible;

    @PrePersist
    protected void onCreate() {
        setVisible(true);
        if (getCreated() == null) {
            setCreated(new Date());
        }
        if (getLastUpdate() == null) {
            setLastUpdate(getCreated());
        }
    }

    @PreUpdate
    protected void onUpdate() {
        setLastUpdate(new Date());
    }

    public @Override boolean equals(final Object obj) {
        if (!(obj instanceof AbstractEntity) || !obj.getClass().equals(getClass())) {
            return false;
        }
        final AbstractEntity entity = (AbstractEntity) obj;
        if ((uuid == null) | (entity.uuid == null)) {
            return false;
        }
        return uuid.equals(entity.uuid);
    }

    public @Override int hashCode() {
        return hasText(uuid) ? uuid.hashCode() + 123412 : super.hashCode();
    }

    public final String getUuid() {
        return uuid;
    }

    public final void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public final Integer getVersion() {
        return version;
    }

    public final void setVersion(final Integer version) {
        this.version = version;
    }

    public final Date getCreated() {
        return created;
    }

    public final void setCreated(final Date created) {
        this.created = created;
    }

    public final Date getLastUpdate() {
        return lastUpdate;
    }

    public final void setLastUpdate(final Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(final boolean visible) {
        this.visible = visible;
    }

}
