package org.internna.ossmoney.model.account;

public enum AccountType {

    DAY_TO_DAY("account.type.daytoday"), SAVING("account.type.saving"), CREDIT_CARD("account.type.cc");

    private final String i18nkey;

    private AccountType(final String i18nkey) {
        this.i18nkey = i18nkey;
    }

    public String getI18nKey() {
        return this.i18nkey;
    }

}
