package org.internna.ossmoney.model;

import java.util.Set;
import java.util.HashSet;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.money.CurrencyUnit;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.internna.ossmoney.model.account.FinancialInstitution;

@Entity
@Table(name = Country.COUNTRY)
public class Country extends AbstractEntity {

	public static final String COUNTRY = "COUNTRY";

	@NotNull
	@Column(name = "I18NKEY", nullable = false, updatable = false)
	private String countryKey;

	@NotNull
	@Column(name = "CURRENCY", nullable = false)
	@Type(type = "org.jadira.usertype.moneyandcurrency.moneta.PersistentCurrencyUnit")
	private CurrencyUnit currency;

	@JsonBackReference
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "country")
	private Set<FinancialInstitution> financialInstitutions = new HashSet<FinancialInstitution>();

	public final String getCountryKey() {
		return countryKey;
	}

	public final void setCountryKey(final String countryKey) {
		this.countryKey = countryKey;
	}

	public final CurrencyUnit getCurrency() {
		return currency;
	}

	public final void setCurrency(final CurrencyUnit currency) {
		this.currency = currency;
	}

	public final Set<FinancialInstitution> getFinancialInstitutions() {
		return financialInstitutions;
	}

	public final void setFinancialInstitutions(final Set<FinancialInstitution> financialInstitutions) {
		this.financialInstitutions = financialInstitutions;
	}

}
