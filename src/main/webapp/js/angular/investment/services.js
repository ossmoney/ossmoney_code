(function(window, angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.factory("Investment", ['$resource', function($resource) {
		return $resource("investment", {}, {
			"query": {
				"method": "GET"
			}
		});
	}]);

	investment.factory("Deposit", ['$resource', function($resource) {
		return $resource("investment/deposit", {}, {
			"save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

	investment.factory("DepositCancellation", ['$resource', function($resource) {
		return $resource("investment/deposit/cancel", {}, {
			"save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

})(window, angular);