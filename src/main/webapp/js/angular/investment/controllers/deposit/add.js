(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyDepositAddEditController", ["$rootScope", "$scope", "$state", "$stateParams", "$translate", "$locale", "FinancialInstitution", "Deposit", "Alert", "ISO", function($rootScope, $scope, $state, $stateParams, $translate, $locale, FinancialInstitution, Deposit, Alert, ISO) {
		$scope.apyValidation = "";
		$scope.nameValidation = "";
		$scope.bankValidation = "";
		$scope.balanceValidation = "";
		$scope.country = $translate.instant("country." + $locale.id);
		$scope.deposit = $stateParams.deposit || {
			"duration": 6,
			"opened": new Date(),
			"annualPercentageYield": 0,
			"balance": {
				"number": 0,
				"currency": $rootScope.currency
			}
		};
		$scope.saveDeposit = function() {
			var depo = angular.copy($scope.deposit);
			delete depo.transactions;
			depo.opened = ISO.asDate(depo.opened);
			depo.balance = Number(depo.balance.number);
			depo.financialInstitution = depo.financialInstitution.uuid;
			depo.account = depo.account !== undefined ? depo.account.uuid : null;
			$scope.nameValidation = !$scope.deposit.name ? "has-error has-feedback" : "";
			$scope.bankValidation = !$scope.deposit.financialInstitution ? "has-error has-feedback" : "";
			$scope.balanceValidation = ($scope.deposit.balance.number === null) || ($scope.deposit.balance.number <= 0) ? "has-error has-feedback" : "";
			$scope.apyValidation = ($scope.deposit.annualPercentageYield === null) || ($scope.deposit.annualPercentageYield <= 0) ? "has-error has-feedback" : "";
			if (!$scope.nameValidation & !$scope.balanceValidation & !$scope.bankValidation & !$scope.apyValidation) {
				var deposit = Deposit.save(depo, function() {
					if (deposit != null) {
						if (depo.account !== null) {
							angular.forEach($rootScope.accounts, function (account) {
								if (account.uuid === depo.account) {
									account.currentBalance.number -= depo.balance;
									$scope.account = account; 
								}
							});
						}
						if ($scope.deposit.uuid !== undefined) {
							var index = $rootScope.portfolio.deposits.map(function(deposit) {
								return deposit.uuid;
							}).indexOf($scope.deposit.uuid);
							$rootScope.portfolio.deposits.splice(index, 1);
						}
						$rootScope.portfolio.deposits.push(deposit);
						$rootScope.$broadcast("portfolio-changed");
						$state.go("account_main");
						Alert.show($translate.instant($scope.deposit.uuid !== undefined ? "deposit.modify.success" : "deposit.add.success"));
					} else {
						Alert.warn($translate.instant($scope.deposit.uuid !== undefined ? "deposit.modify.error" : "deposit.add.error"));
					}
				}, function(err) {
					Alert.warn($translate.instant($scope.deposit.uuid !== undefined ? "deposit.modify.error" : "deposit.add.error"));
				});
			}
		};
		$scope.open = function($event) {
			$scope.opened = true;
			$event.preventDefault();
			$event.stopPropagation();
		};
		$scope.findFinancialInstitutions = function(country, callback) {
			var banks = FinancialInstitution.query({
				"country": country
			}, function() {
				callback(banks);
				$rootScope.banks[country] = banks;
			});
		};
		if ($rootScope.banks[$scope.country] === undefined) {
			$scope.findFinancialInstitutions($scope.country, function(banks) {
				if ($scope.deposit.uuid === undefined) {
					$scope.deposit.financialInstitution = banks.length > 0 ? banks[0] : null;
				}
			});
		} else if ($scope.deposit.uuid === undefined) {
			$scope.deposit.financialInstitution = $rootScope.banks[$scope.country].length > 0 ? $rootScope.banks[$scope.country][0] : null;
		}
	}]);

})(angular);