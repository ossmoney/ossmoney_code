(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyDepositChartController", ["$rootScope", "$scope", "$translate", function($rootScope, $scope, $translate) {
		$scope.recalculateDeposits = function() {
			$scope.data = [];
			$scope.labels = [];
			$scope.deposits = 0;
			if ($rootScope.portfolio !== undefined) {
				angular.forEach($rootScope.portfolio.deposits, function(deposit) {
					$scope.labels.push(deposit.name);
					$scope.data.push(deposit.balance.number);
					$scope.deposits += deposit.balance.number;
				});
			}
			if ($scope.data.length === 0) {
				$scope.data.push(1);
				$scope.labels.push($translate.instant("investment.deposits.no_data"));
			}
		};
		$scope.recalculateDeposits();
		$rootScope.$on("portfolio-changed", function() {
			$scope.recalculateDeposits();
		});
	}]);

})(angular);