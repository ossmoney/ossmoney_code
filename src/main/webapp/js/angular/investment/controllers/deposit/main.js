(function(angular) {

	var investment = angular.module("ossmoneyAccount");

	investment.controller("OssmoneyDepositDetailsController", ["$rootScope", "$scope", "$state", "$stateParams", "$translate", "DepositCancellation", "Array", "Alert", function($rootScope, $scope, $state, $stateParams, $translate, DepositCancellation, Array, Alert) {
		$scope.opened = false;
		$scope.loading = false;
		$scope.today = new Date();
		$scope.cancellation = true;
		$scope.dateValidation = "";
		$scope.balanceValidation = "";
		$scope.cancellationType = "TOTAL";
		$scope.deposit = $stateParams.deposit;
		$scope.cancellationData = {
			"opened": $scope.today,
			"uuid": $scope.deposit.uuid,
			"balance": -$scope.deposit.balance.number
		};
		$scope.open = function($event) {
			$scope.opened = true;
			$event.preventDefault();
			$event.stopPropagation();
		};
		$scope.setAmount = function(amount) {
			$scope.cancellationData.balance = -amount;
		};
		$scope.showCancellation = function() {
			$scope.cancellation = false;
		};
		$scope.withdraw = function() {
			$scope.dateValidation = !$scope.cancellationData.opened? "has-error has-feedback" : "";
			$scope.balanceValidation = ($scope.cancellationData.balance === null) || ($scope.cancellationData.balance >= 0) ? "has-error has-feedback" : "";
			if (!$scope.balanceValidation & !$scope.dateValidation) {
				$scope.loading = true;
				var deposit = DepositCancellation.save($scope.cancellationData, function() {
					$scope.loading = false;
					if (deposit && deposit.balance) {
						Array.remove($rootScope.portfolio.deposits, $scope.cancellationData.uuid);
						if (deposit.balance && deposit.balance.number > 0) {
							$rootScope.portfolio.deposits.push(deposit);
						}
						$rootScope.$broadcast("portfolio-changed");
						$state.go("account_main");
						Alert.show($translate.instant("deposit.cancellation.success." + $scope.cancellationType));
					} else {
						Alert.warn($translate.instant("deposit.cancellation.error"));
					}
				}, function(err) {
					$scope.loading = false;
					Alert.warn($translate.instant("deposit.cancellation.error"));
				});
			}
		};
	}]);

})(angular);
