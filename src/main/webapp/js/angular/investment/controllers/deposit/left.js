(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyDepositDetailsLeftController", ["$rootScope", "$scope", "$stateParams", "$translate", function($rootScope, $scope, $stateParams, $translate) {
		$scope.deposit = $stateParams.deposit;
		$scope.fromDate = new Date($scope.deposit.opened);
		$scope.toDate = new Date($scope.fromDate.getTime());
		$scope.toDate.setMonth($scope.toDate.getMonth() + $scope.deposit.duration);
		$scope.recalculateTotals = function() {
			var millisInDay = 24 * 60 * 60 * 1000;
			$scope.days = Math.round(($scope.toDate.getTime() - $scope.fromDate.getTime()) / millisInDay) - 1;
		};
		$rootScope.$on("portfolio-changed", function() {
			$scope.recalculateTotals();
		});
		$scope.recalculateTotals();
	}]);

})(angular);