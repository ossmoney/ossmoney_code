(function(angular) {

	var investment = angular.module("ossmoneyInvestment");

	investment.controller("OssmoneyPreciousMetalChartController", ["$rootScope", "$scope", "$translate", function($rootScope, $scope, $translate) {
		$scope.recalculatePreciousMetals = function() {
			$scope.data = [];
			$scope.labels = [];
			$scope.preciousMetals = 0;
			if ($rootScope.portfolio !== undefined) {
				angular.forEach($rootScope.portfolio.preciousMetals, function(preciousMetal) {
					var price = (preciousMetal.price.spot.number * preciousMetal.weight).toFixed(2);
					$scope.labels.push($translate.instant(preciousMetal.preciousMetalType));
					$scope.data.push(price);
					$scope.preciousMetals += price;
				});
			}
			if ($scope.data.length === 0) {
				$scope.data.push(1);
				$scope.labels.push($translate.instant("investment.preciousMetal.no_data"));
			}
		};
		$scope.recalculatePreciousMetals();
		$rootScope.$on("portfolio-changed", function() {
			$scope.recalculatePreciousMetals();
		});
	}]);

})(angular);