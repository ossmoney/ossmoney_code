(function(angular) {

	var app = angular.module("ossmoney");

	app.controller("OssmoneyInitController", ["$state", "selection", function($state, selection) {
		$state.go(selection);
	}]);

	app.controller("OssmoneyPreferencesController", ["$rootScope", "$scope", "$state", "$translate", "tmhDynamicLocale", "ChartColor", "User", "Alert", function($rootScope, $scope, $state, $translate, tmhDynamicLocale, ChartColor, User, Alert) {
		$scope.user = {
			"skin": $rootScope.skin,
			"locale": $rootScope.locale,
			"currency": $rootScope.currency.currencyCode
		};
		$scope.changeTheme = function(theme) {
			$scope.user.skin = theme;
		};
		$scope.savePreferences = function(theme) {
			var modified = User.rest.save($scope.user, function() {
				if (modified !== null) {
					User.configure($rootScope, $translate, tmhDynamicLocale, ChartColor, modified);
					$state.go("account_main");
					Alert.show($translate.instant("save.preferences.success"));
				} else {
					Alert.warn($translate.instant("save.preferences.error"));
				}
			});
		};
	}]);

})(angular);