(function(angular) {

	var menu = angular.module("ossmoneyMenu");

	menu.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"menu_dashboard" : "Ingresos y gastos",
			"menu_account" : "Cuentas y depósitos",
			"menu_investment": "Cartera de inversiones",
			"menu_budget" : "Presupuestos",
			"menu_forecast" : "Metas de ahorro"
		});
		$translateProvider.translations('en', {
			"menu_dashboard" : "Income vs expenses",
			"menu_account" : "Accounts",
			"menu_investment": "Investment portfolio",
			"menu_budget" : "Budgets",
			"menu_forecast" : "Forecast planning"
		});
	}]);

})(angular);