(function(angular) {

	var admin = angular.module("ossmoneyAdmin");

	admin.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"entities.add.title": "Añadir entidad financiera",
			"entity.name": "Nombre de la entidad",
			"entity.web": "URL web",
			"entity.create": "Crear entidad financiera",
			"entity.create.success": "Entidad dad de alta con éxito",
			"entity.create.error": "Error al dar de alta la entidad financiera"
		});
		$translateProvider.translations('en', {
			"entities.add.title": "Add financial entity",
			"entity.name": "Name",
			"entity.web": "Web url",
			"entity.create": "Create bank",
			"entity.create.success": "Financial entity created",
			"entity.create.error": "Error creating financial entity"
		});
	}]);

})(angular);
