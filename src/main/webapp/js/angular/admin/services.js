(function(window, angular) {

	var account = angular.module("ossmoneyAccount");

	account.factory("Entity", ['$resource', function($resource) {
		return $resource("admin/banks", {}, {
			"save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

})(window, angular);