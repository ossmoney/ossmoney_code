(function(window, angular) {

	var app = angular.module("ossmoney");

	app.factory("User", ['$resource', function($resource) {
		return {
			"rest": $resource("user", {}, {
				"query": {
					"method": "GET"
				}, "save": {
					"method": "POST",
					"headers": {
						"X-CSRF-TOKEN": window._csrf
					}
				}
			}), "configure": function($rootScope, $translate, tmhDynamicLocale, ChartColor, user) {
				$translate.use(user.locale);
				$rootScope.skin = user.skin;
				$rootScope.locale = user.locale;
				$rootScope.admin = user.administrator;
				if (ChartColor[user.skin] !== undefined) {
					Chart.defaults.global.colours = ChartColor[user.skin](); 
				}
				if (user.locale === "es_ES") {
					tmhDynamicLocale.set(user.locale);
				}
				$rootScope.currency = user.currency;
			}
		};
	}]);

	app.factory("Array", [function() {
		return {
			"find": function(array, uuid) {
				var found = -1;
				if (uuid && array && array.length) {
					found = array.map(function(value) {
						return value.uuid;
					}).indexOf(uuid);
				}
				return found;
			}, "remove": function(array, uuid) {
				var index = this.find(array, uuid);
				if (!isNaN(index) && ((index >= 0) & (index < array.length))) {
					array.splice(index, 1);
				}
			}
		};
	}]);

	app.factory("ISO", [function() {
		return {
			"asDate": function(date) {
				var converted = date != null ?  typeof date === "string" || !isNaN(date) ? new Date(date) : date : null;
				return converted !== null ? converted.getFullYear() + "-" + (converted.getMonth() + 1) + "-" + converted.getDate() : null;
			}
		};
	}]);

	app.factory("Alert", ["$rootScope", "$timeout", function($rootScope, $timeout) {
		return {
			"show": function(text) {
				this._toggle("success", text, "glyphicon-ok");
			}, "warn": function(text) {
				this._toggle("danger", text, "glyphicon-bell");
			}, "_toggle": function(type, text, icon) {
				$rootScope.alert.type = type;
				$rootScope.alert.icon = icon;
				$rootScope.alert.message = text;
				$rootScope.alert.hide = false;
				$timeout(function() {
					$rootScope.alert.hide = true;
				}, 2000);
			}
		};
	}]);

	app.factory("ChartColor", [function() {
		return {
			"brown": function() {
				return [
				    { // light grey
				        fillColor: "rgba(220,220,220,0.2)",
				        strokeColor: "rgba(220,220,220,1)",
				        pointColor: "rgba(220,220,220,1)",
				        pointStrokeColor: "#fff",
				        pointHighlightFill: "#fff",
				        pointHighlightStroke: "rgba(220,220,220,0.8)"
				    }, { // light brown
				        fillColor: "rgba(217,178,113,0.2)",
				        strokeColor: "rgba(217,178,113,1)",
				        pointColor: "rgba(217,178,113,1)",
				        pointStrokeColor: "#fff",
				        pointHighlightFill: "#fff",
				        pointHighlightStroke: "rgba(217,178,113,0.8)"
				    }, { // red
				        fillColor: "rgba(227,80,50,0.2)",
				        strokeColor: "rgba(227,80,50,1)",
				        pointColor: "rgba(227,80,50,1)",
				        pointStrokeColor: "#fff",
				        pointHighlightFill: "#fff",
				        pointHighlightStroke: "rgba(227,80,50,0.8)"
				    }, { // yellow
				        fillColor: "rgba(245,242,152,0.2)",
				        strokeColor: "rgba(245,242,152,1)",
				        pointColor: "rgba(245,242,152,1)",
				        pointStrokeColor: "#fff",
				        pointHighlightFill: "#fff",
				        pointHighlightStroke: "rgba(245,242,152,0.8)"
				    }, { // pink
				        fillColor: "rgba(227,188,222,0.2)",
				        strokeColor: "rgba(227,188,222,1)",
				        pointColor: "rgba(227,188,222,1)",
				        pointStrokeColor: "#fff",
				        pointHighlightFill: "#fff",
				        pointHighlightStroke: "rgba(227,188,222,1)"
				    }, { // blue
				        fillColor: "rgba(151,187,205,0.2)",
				        strokeColor: "rgba(151,187,205,1)",
				        pointColor: "rgba(151,187,205,1)",
				        pointStrokeColor: "#fff",
				        pointHighlightFill: "#fff",
				        pointHighlightStroke: "rgba(151,187,205,0.8)"
				    }, { // light green
				        fillColor: "rgba(185,214,148,0.2)",
				        strokeColor: "rgba(185,214,148,1)",
				        pointColor: "rgba(185,214,148,1)",
				        pointStrokeColor: "#fff",
				        pointHighlightFill: "#fff",
				        pointHighlightStroke: "rgba(185,214,148,0.8)"
				    }, { // light purple
				        fillColor: "rgba(194,177,222,0.2)",
				        strokeColor: "rgba(194,177,222,1)",
				        pointColor: "rgba(194,177,222,1)",
				        pointStrokeColor: "#fff",
				        pointHighlightFill: "#fff",
				        pointHighlightStroke: "rgba(194,177,222,0.8)"
				    }
				];
			}
		};
	}]);

})(window, angular);