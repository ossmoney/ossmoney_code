(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountDetailsLeftController", ["$rootScope", "$scope", "$stateParams", "$translate", function($rootScope, $scope, $stateParams, $translate) {
		$scope.account = $stateParams.account;
		$scope.recalculateTotals = function() {
			var categories = {};
			var income = 0, expenses = 0;
			$scope.incomePercent = 50;
			$scope.expensesPercent = 50;
			$scope.maxCategory = {"name": "", "value": 0, "currency": "EUR"};
			$scope.maxTransaction = {"memo": "", "amount": {"number": 0}, "currency": {"currencyCode" : "EUR"}};
			var transactions = $rootScope.transactions[$scope.account.uuid];
			if (transactions !== undefined) {
				angular.forEach(transactions, function(query) {
					$scope.toDate = query.to;
					$scope.fromDate = query.from;
					angular.forEach(query.transactions, function(transaction) {
						var val = Math.abs(transaction.amount.number);
						if (transaction.transactionType === "INCOME") {
							income += val;
						} else if (transaction.transactionType === "EXPENSE") {
							expenses += val;
							if (-val <= $scope.maxTransaction.amount.number) {
								$scope.maxTransaction = transaction;
							}
							angular.forEach(transaction.tags, function(tag) {
								if (categories[tag.tag] === undefined) {
									categories[tag.tag] = {
										"value": 0,
										"icon": tag.icon,
										"tag": $translate.instant(tag.tag),
										"currency": transaction.amount.currency.currencyCode
									};
								}
								categories[tag.tag].value += val;
							});
						}
					});
				});
				angular.forEach(Object.keys(categories), function(category) {
					if (categories[category].value >= $scope.maxCategory.value) {
						$scope.maxCategory = categories[category];
					}
				});
			}
			if (income + expenses !== 0) {
				$scope.incomePercent = Math.round((100 * income/ (income + expenses))).toFixed(0);
				$scope.expensesPercent = Math.round((100 * expenses / (income + expenses))).toFixed(0);
			}
		};
		$rootScope.$on("transactions-changed", function() {
			$scope.recalculateTotals();
		});
		$scope.recalculateTotals();
	}]);

})(angular);