(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountDetailsController", ["$rootScope", "$scope", "$state", "$stateParams", "$translate", "Transaction", "Reconciliation", "Array", "ISO", "Alert", function($rootScope, $scope, $state, $stateParams, $translate, Transaction, Reconciliation, Array, ISO, Alert) {
		$scope.transactions = [];
		$scope.rowCollection = [];
		$scope.fillTransactions = function() {
			$scope.transactions = [];
			angular.forEach($rootScope.transactions[$scope.account.uuid], function(query) {
				$scope.transactions = $scope.transactions.concat(query.transactions); 
			});
			var last = new Date($scope.fromDate.getTime());
			last.setMonth(last.getMonth() - 1);
			$scope.month1 = $translate.instant("month." + last.getMonth());
			last.setMonth(last.getMonth() - 2);
			$scope.month3 = $translate.instant("month." + last.getMonth());
			last.setMonth(last.getMonth() - 3);
			$scope.month6 = $translate.instant("month." + last.getMonth());
		};
		$scope.loadTransactions = function() {
			var transactions = Transaction.query({
				"account": $scope.account.uuid,
				"to": ISO.asDate($scope.toDate),
				"from": ISO.asDate($scope.fromDate)
			}, function() {
				var search = {
					"to": $scope.toDate,
					"from": $scope.fromDate,
					"transactions" : []
				};
				angular.forEach(transactions, function(transaction) {
					angular.forEach(transaction.tags, function (tag) {
						tag.tags = [];
						tag.text = $translate.instant(tag.tag);
						angular.forEach(tag.categories, function (category) {
							tag.tags.push({
								"tag": category.tag,
								"icon": category.icon, 
								"text": $translate.instant(category.tag)
							});
						});
						delete tag.categories;
					});
					search.transactions.push(transaction);
				});
				$rootScope.transactions[$scope.account.uuid].push(search);
				$scope.fillTransactions();
				$rootScope.$broadcast("transactions-changed");
			});
		};
		$scope.setTransactionsInterval = function(months) {
			if ((months === 0) | (months === 12)) {
				$scope.toDate = new Date();
				$scope.fromDate = new Date();
				$scope.fromDate.setMonth($scope.fromDate.getMonth() - months - 1);
			} else {
				$scope.toDate = new Date($scope.fromDate.getTime());
				$scope.fromDate.setMonth($scope.fromDate.getMonth() - months);
			}
			$scope.loadTransactions();
		};
		$scope.reconcileMovements = function() {
			$scope.loading = true;
			var accounts = Reconciliation.save({
				"account": $scope.account.uuid,
				"chargeAccount": $scope.reconcile.account ? $scope.reconcile.account.uuid : null,
				"transactions": Object.keys($scope.reconcile.selection).map(function (key) {
					return $scope.reconcile.selection[key].uuid;
				})
			}, function() {
				$scope.loading = false;
				Array.remove($rootScope.accounts, accounts[0].uuid);
				$rootScope.accounts.push(accounts[0]);
				$rootScope.transactions[accounts[0].uuid] = [];
				if (accounts.length > 1) {
					Array.remove($rootScope.accounts, accounts[1].uuid);
					$rootScope.accounts.push(accounts[1]);
					$rootScope.transactions[accounts[1].uuid] = [];
				}
				$rootScope.$broadcast("accounts-changed");
				$state.go("account_main");
				Alert.show($translate.instant("transactions.reconcile.success"));
			}, function(err) {
				$scope.loading = false;
				Alert.warn($translate.instant("transactions.reconcile.error"));
			});
		};

		$scope.account = $stateParams.account;
		if ($scope.account.creditCard) {
			$scope.clearReconciliation = function() {
				if ($scope.reconcile.amount !== 0) {
					$scope.reconcile.amount = 0;
					$scope.reconcile.selection = {};
					var cks = document["transactionForm"].getElementsByTagName('input');
					for (var i = 0; i < cks.length; i++)  {
						if (cks[i].type == 'checkbox')   {
							cks[i].checked = false;
						}
					}
				}
			};
			$scope.addMovementToReconcile = function(transaction) {
				if ($scope.reconcile.selection[transaction.uuid]) {
					delete $scope.reconcile.selection[transaction.uuid]
				} else {
					$scope.reconcile.selection[transaction.uuid] = transaction;
				}
				$scope.reconcile.amount = 0;
				angular.forEach($scope.reconcile.selection, function(transaction) {
					$scope.reconcile.amount += transaction.amount.number;
				});
			};
			$scope.reconcile = {
				"amount": 0,
				"account": null,
				"selection": {}
			};
		}
		var lastQuery = $rootScope.transactions[$scope.account.uuid];
		if ((lastQuery === undefined) || (lastQuery.length === 0)) {
			$scope.setTransactionsInterval($scope.account.creditCard ? 12 : 0);
		} else {
			lastQuery = lastQuery[lastQuery.length - 1];
			$scope.toDate = lastQuery.to;
			$scope.fromDate = lastQuery.from;
			if (($rootScope.transactions[$scope.account.uuid] === undefined) || ($rootScope.transactions[$scope.account.uuid].length === 0)) {
				$scope.loadTransactions();
			} else {
				$scope.fillTransactions();
			}
		}
	}]);

})(angular);
