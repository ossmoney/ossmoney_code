(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountListController", ["$rootScope", "$scope", "$state", function($rootScope, $scope, $state) {
		$scope.loading = $rootScope.accounts === undefined;
		$rootScope.lateralMenu = $rootScope.lateralMenuOptions[0];
		$scope.recalculateAccountNumbers = function() {
			$scope.creditCardAccounts = $rootScope.accounts.reduce(function(cc, account) {
				return cc + account.creditCard ? 1 : 0;
			}, 0);
			$scope.bankAccounts = $rootScope.accounts.length - $scope.creditCardAccounts;
		}
		$rootScope.$on("accounts-changed", function() {
			$scope.loading = false;
			$scope.recalculateAccountNumbers();
		});
		$scope.recalculateAccountNumbers();
	}]);

})(angular);