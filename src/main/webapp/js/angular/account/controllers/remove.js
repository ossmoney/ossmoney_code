(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountCloseDeleteController", ["$scope", "$modal", function($scope, $modal) {
		$scope.showModal = function () {
			var modalInstance = $modal.open({
				"animation": "true",
				"templateUrl": "account.remove.modal.html",
				"controller": 'OssmoneyAccountCloseDeleteModalController',
				"resolve": {
					"account": function() {
						return $scope.account;
					}
		        }
		    });
		};
	}]);

	account.controller("OssmoneyAccountCloseDeleteModalController", ["$rootScope", "$scope", "$modalInstance", "$state", "$translate", "Alert", "Array", "Account", "account",  function($rootScope, $scope, $modalInstance, $state, $translate, Alert, Array, Account, account) {
		$scope.typed = "";
		$scope.loading = false;
		$scope.account = account;
		$scope.action = $scope.account.closed !== null ? $translate.instant("account.delete.type") : $translate.instant("account.close.type");
		$scope.cancel = function () {
			$scope.loading = false;
			$modalInstance.dismiss('cancel');
		};
		$scope.remove = function() {
			$scope.loading = true;
			var account = Account.remove({
				"uuid" : $scope.account.uuid
			}, function() {
				var uuid = $scope.account.uuid;
				$modalInstance.dismiss('cancel');
				Array.remove($rootScope.accounts, uuid);
				if (account && account.closed) {
					$scope.account = account;
					$rootScope.accounts.push(account);
					Alert.show($translate.instant("account.close.success"));
				} else {
					Alert.show($translate.instant("account.delete.success"));
				}
				$state.go("account_main");
				$rootScope.$broadcast("accounts-changed");
			}, function (err) {
				$modalInstance.dismiss('cancel');
				Alert.warn($translate.instant("account.delete.error"));
			});
		};
	}]);

})(angular);