(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.controller("OssmoneyAccountMenuController", ["$rootScope", "$scope", "$stateParams", function($rootScope, $scope, $stateParams) {
		$scope.highlight = function (options, suboption) {
			if (options !== undefined) {
				angular.forEach(options, function(option) {
					option.active = false;
				});
			}
			if (suboption !== undefined) {
				suboption.active = true;
			}
		};
		$scope.clearSubmenus = function (options, click) {
			if ($scope.submenus === undefined) {
				$scope.submenus = {};
			}
			angular.forEach($scope.submenus, function(submenu) {
				$scope.submenus[submenu] = false;
			});
			if (click && click !== "none") {
				$scope.highlight(options);
				$scope.submenus[click] = true;
			}
		};
		$scope.switchOption = function(menu, click, submenu) {
			$rootScope.lateralMenu.active = "";
			menu.active = "active";
			$rootScope.lateralMenu = menu;
			$scope.clearSubmenus(menu.options, click);
			if (submenu !== undefined) {
				angular.forEach(menu.options, function(option) {
					option.active = option.navigation === submenu ? "active" : "";
				});
			}
		};
		var menu = $rootScope.lateralMenuOptions[$stateParams.menu - 1];
		$scope.switchOption(menu, menu.onclick, $stateParams.submenu);
	}]);

})(angular);