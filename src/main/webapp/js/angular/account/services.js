(function(window, angular) {

	var account = angular.module("ossmoneyAccount");

	account.factory("Account", ['$resource', function($resource) {
		return $resource("account/:uuid", {}, {
			"query": {
				isArray: true
			}, "save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}, "remove": {
				"method": "DELETE",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

	account.factory("AccountType", ['$resource', function($resource) {
		return $resource("account/type", {}, {
			"query": {
				isArray: true
			}
		});
	}]);

	account.factory("Tags", ['$resource', function($resource) {
		return $resource("transaction/tags", {}, {
			"query": {
				isArray: true
			}
		});
	}]);

	account.factory("FinancialInstitution", ['$resource', function($resource) {
		return $resource("banks/:country", {}, {
			"query": {
				isArray: true
			}
		});
	}]);

	account.factory("Reopen", ['$resource', function($resource) {
		return $resource("account/reopen/:uuid", {}, {
			"save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

	account.factory("Transaction", ['$resource', function($resource) {
		return $resource("transaction/:account/:from/:to", {}, {
			"query": {
				isArray: true
			}, "save": {
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}, "remove": {
				"method": "DELETE",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

	account.factory("Reconciliation", ['$resource', function($resource) {
		return $resource("transaction/reconciliation", {}, {
			"save": {
				isArray: true,
				"method": "POST",
				"headers": {
					"X-CSRF-TOKEN": window._csrf
				}
			}
		});
	}]);

})(window, angular);