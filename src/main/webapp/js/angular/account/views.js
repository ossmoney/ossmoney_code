(function(angular) {

	var account = angular.module("ossmoneyAccount");

	account.config(["$stateProvider", function($stateProvider) {
		$stateProvider.state("account_main", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyAccountMenuController",
					"templateUrl": "views/account/menu.view.html"
				}, "account_list": {
					"templateUrl": "views/account/list.view.html"
				}
			}, "params": {
				"menu": 1
			}
		}).state("account_add", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyAccountMenuController",
					"templateUrl": "views/account/menu.view.html"
				}, "account_list": {
					"controller": "OssmoneyAccountAddController",
					"templateUrl": "views/account/add.view.html"
				}
			}, "params": {
				"menu": 2
			}
		}).state("account_edit", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyAccountMenuController",
					"templateUrl": "views/account/menu.view.html"
				}, "account_list": {
					"controller": "OssmoneyAccountAddController",
					"templateUrl": "views/account/add.view.html"
				}
			}, "params": {
				"menu": 2,
				"account": null
			}
		}).state("account_details", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyAccountDetailsLeftController",
					"templateUrl": "views/account/details/left.view.html"
				}, "account_list": {
					"controller": "OssmoneyAccountDetailsController",
					"templateUrl": "views/account/details/main.view.html"
				}
			}, "params": {
				"account": null
			}
		}).state("transaction_add", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyAccountMenuController",
					"templateUrl": "views/account/menu.view.html"
				}, "account_list": {
					"controller": "OssmoneyTransactionAddController",
					"templateUrl": "views/account/transaction.main.view.html"
				}
			}, "params": {
				"menu": 1,
				"account": null,
				"transaction": null
			}
		}).state("transaction_edit", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyAccountMenuController",
					"templateUrl": "views/account/menu.view.html"
				}, "account_list": {
					"controller": "OssmoneyTransactionAddController",
					"templateUrl": "views/account/transaction.main.view.html"
				}
			}, "params": {
				"menu": 1,
				"account": null,
				"transaction": null
			}
		}).state("deposit_add", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyAccountMenuController",
					"templateUrl": "views/account/menu.view.html"
				}, "account_list": {
					"controller": "OssmoneyDepositAddEditController",
					"templateUrl": "views/investment/deposit/add.view.html"
				}
			}, "params": {
				"menu": 3,
				"deposit": null
			}
		}).state("deposit_details", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyDepositDetailsLeftController",
					"templateUrl": "views/investment/deposit/left.view.html"
				}, "account_list": {
					"controller": "OssmoneyDepositDetailsController",
					"templateUrl": "views/investment/deposit/main.view.html"
				}
			}, "params": {
				"deposit": null
			}
		}).state("preferences", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyAccountMenuController",
					"templateUrl": "views/account/menu.view.html"
				}, "account_list": {
					"controller": "OssmoneyPreferencesController",
					"templateUrl": "views/preferences.view.html"
				}
			}, "params": {
				"menu": 4
			}
		}).state("entities", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyAccountMenuController",
					"templateUrl": "views/account/menu.view.html"
				}, "account_list": {
					"controller": "OssmoneyAdminEntitiesController",
					"templateUrl": "views/admin/entities.view.html"
				}
			}, "params": {
				"menu": 5,
				"submenu": "entities"
			}
		}).state("metals", {
			"parent": "account",
			"views": {
				"account_menu": {
					"controller": "OssmoneyAccountMenuController",
					"templateUrl": "views/account/menu.view.html"
				}, "account_list": {
					"controller": "OssmoneyAdminMetalsController",
					"templateUrl": "views/admin/metals.view.html"
				}
			}, "params": {
				"menu": 5,
				"submenu": "metals"
			}
		});
	}]);

})(angular);