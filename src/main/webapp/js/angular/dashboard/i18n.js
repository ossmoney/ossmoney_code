(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard");

	dashboard.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"dashboard.networth": "Patrimonio neto",
			"dashboard.networth.accounts": "Patrimonio neto en cuentas y tarjetas",
			"dashboard.no_accounts": "Cliente sin cuentas",
			"dashboard.ivse": "Ingresos vs Gastos",
			"dashboard.income": "Ingresos",
			"dashboard.expenses": "Gastos",
			"dashboard.expenses.time": "Evolución de los gastos",
			"dashboard.expenses.category": "Gastos por categoría",
			"dashboard.date.0": "Mes en curso",
			"dashboard.date.1": "Últimos 30 días",
			"dashboard.date.3": "Último trimestre",
			"dashboard.date.6": "Último semestre",
			"dashboard.total.income": "Total ingresos",
			"dashboard.total.expenses": "Total gastos",
			"dashboard.total.credit": "Crédito dispuesto"
		});
		$translateProvider.translations('en', {
			"dashboard.networth": "Net worth",
			"dashboard.networth.accounts": "Net worth in accounts & credit cards",
			"dashboard.no_accounts": "No accounts",
			"dashboard.ivse": "Income vs Expenses",
			"dashboard.income": "Income",
			"dashboard.expenses": "Expenses",
			"dashboard.expenses.time": "Expenses timeline",
			"dashboard.expenses.category": "Expenses by category",
			"dashboard.date.0": "Current month",
			"dashboard.date.1": "Last 30 days",
			"dashboard.date.3": "Last quarter",
			"dashboard.date.6": "Last six months",
			"dashboard.total.income": "Total income",
			"dashboard.total.expenses": "Total expenses",
			"dashboard.total.credit": "Pending credit"
		});
	}]);

	dashboard.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"AUTOMOBILE": "Automóvil",
			"CAR_INSURANCE": "Seguro de coche",
			"CAR_RENT": "Alquiler de coche",
			"GASOLINE": "Gasolina",

			"CLOTHING": "Ropa y complementos",

			"FINANCIAL": "G. financieros",
			"INTEREST": "Intereses",
			"BANK_CHARGE": "Comisiones bancarias",

			"HEALTHCARE": "Cuidado de la salud",
			"VISION": "Gafas, lentillas",
			"PHYSICIAN": "Medicina/fisioterapia",

			"HOUSEHOLD": "Hogar",
			"RENT": "Alquiler",

			"INSURANCE": "Seguros",

			"JOB": "Trabajo",
			"PAYCHECK": "Nómina",

			"LEISURE": "Ocio",
			"VIDEOGAMES": "Videojuegos",
			"CINEMA": "Cine, teatro, conciertos",

			"MISC": "Gastos varios",
			"GIFTS": "Regalos",
			"CASH_ATM": "Cajeros automáticos",

			"TAXES": "Tasas e impuestos",
			"GOVERMENT_TAX": "Impuestos" 
		});
		$translateProvider.translations('en', {
			"AUTOMOBILE": "Automobile",
			"CAR_INSURANCE": "Car insurance",
			"CAR_RENT": "Car rent",
			"GASOLINE": "Gasoline",

			"CLOTHING": "Clothing",

			"FINANCIAL": "Financial",
			"INTEREST": "Interest",
			"BANK_CHARGE": "Bank charge",

			"HEALTHCARE": "Healthcare",
			"VISION": "Vision",
			"PHYSICIAN": "Physician",

			"HOUSEHOLD": "Household",
			"RENT": "Rent",

			"INSURANCE": "Insurance",

			"JOB": "Job",
			"PAYCHECK": "Paycheck",

			"LEISURE": "Leisure",
			"VIDEOGAMES": "Videogames",
			"CINEMA": "Cinema, theater, concerts",

			"MISC": "Miscellaneous expenses",
			"CASH_ATM": "ATM",
			"GIFTS": "Gifts",

			"TAXES": "Taxes",
			"GOVERMENT_TAX": "Taxes" 
		});
	}]);

	dashboard.config(["$translateProvider", function($translateProvider) {
		$translateProvider.translations('es', {
			"month.0": "Enero",
			"month.1": "Febrero",
			"month.2": "Marzo",
			"month.3": "Abril",
			"month.4": "Mayo",
			"month.5": "Junio",
			"month.6": "Julio",
			"month.7": "Agosto",
			"month.8": "Septiembre",
			"month.9": "Octubre",
			"month.10": "Noviembre",
			"month.11": "Diciembre"
		});
		$translateProvider.translations('en', {
			"month.0": "January",
			"month.1": "February",
			"month.2": "March",
			"month.3": "April",
			"month.4": "May",
			"month.5": "June",
			"month.6": "July",
			"month.7": "August",
			"month.8": "September",
			"month.9": "October",
			"month.10": "November",
			"month.11": "December"
		});
	}]);

})(angular);