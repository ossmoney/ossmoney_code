(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard");

	dashboard.factory("IncomeVsExpenses", ['$resource', function($resource) {
		return $resource("transaction/income-and-expenses/:months", {}, {
			"query": {
				isArray: true
			}
		});
	}]);

	dashboard.factory("ExpensesService", function() {
		return {
			"days": function(date) {
				return Number(date.substring(0, 4)) * 12 * 30 + Number(date.substring(5, 7)) * 30 + Number(date.substring(8, 10));
			}, "bucket": function($rootScope, date, currentMonth) {
				var month = currentMonth - Number(date.substring(5,7));
				return month < 0 ? month + 12 : month;
			}, "inInterval": function(date, today, range) {
				return (range * 30) >= (today - this.days(date));
			}, "groupByMonth": function($rootScope, transaction, currentMonth) {
				var bucket = this.bucket($rootScope, transaction.operationDate, currentMonth);
				$rootScope.timeline[$rootScope.timeline.length - bucket - 1][transaction.transactionType] += Math.abs(transaction.amount.number);
			}, "categorize": function($rootScope, $translate, transaction) {
				var tags = transaction.tags || [];
				angular.forEach(tags, function(tag) {
					var categories = tag.categories || [];
					angular.forEach(categories, function(category) {
						if ($rootScope.expensesByCategory[category.tag] === undefined) {
							$rootScope.expensesByCategory[category.tag] = {
								"items": [],
								"balance": 0,
								"tag": $translate.instant(category.tag)
							};
						}
						$rootScope.expensesByCategory[category.tag].items.push(transaction);
						$rootScope.expensesByCategory[category.tag].balance += Math.abs(transaction.amount.number);
					});
				});
			}, "categorizeByTime": function($rootScope, $translate, transaction, currentMonth) {
				var tags = transaction.tags || [];
				var bucket = this.bucket($rootScope, transaction.operationDate, currentMonth);
				var expensesByCategoryMonth = $rootScope.expensesByCategoryOverTime[$rootScope.expensesByCategoryOverTime.length - bucket - 1];
				angular.forEach(tags, function(tag) {
					var categories = tag.categories || [];
					angular.forEach(categories, function(category) {
						if (expensesByCategoryMonth[category.tag] === undefined) {
							expensesByCategoryMonth[category.tag] = {
								"items": [],
								"balance": 0,
								"tag": $translate.instant(category.tag)
							};
						}
						expensesByCategoryMonth[category.tag].items.push(transaction);
						expensesByCategoryMonth[category.tag].balance += Math.abs(transaction.amount.number);
					});
				});
			}
		};
	});

})(angular);