(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard");

	dashboard.controller("OssmoneyDashboardInitController", ["$rootScope", "$scope", "$translate", function($rootScope, $scope, $translate) {
		$rootScope.maxMonthsLoaded = 0;
		$scope.toggleDropdown = function($event) {
			$event.preventDefault();
			$event.stopPropagation();
		};
		$scope.changeInterval = function(months) {
			$rootScope.$broadcast("interval-changed", {
				"months": months
			});
		};
		$scope.changeInterval(3);
	}]);

})(angular);