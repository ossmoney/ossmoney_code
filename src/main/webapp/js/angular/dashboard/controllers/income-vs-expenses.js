(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard");

	dashboard.controller("OssmoneyDashboardIncomeVsExpensesController", ["$rootScope", "$scope", "$translate", function($rootScope, $scope, $translate) {
		$scope.recalculateIncomeVsExpenses = function() {
			$scope.labels_ivse = [];
			$scope.data_ivse = [[], []];
			$scope.series_ivse = [$translate.instant("dashboard.income"), $translate.instant("dashboard.expenses")];
			angular.forEach($rootScope.timeline, function(data) {
				$scope.labels_ivse.push(data["month"]);
				$scope.data_ivse[0].push(data["INCOME"]);
				$scope.data_ivse[1].push(data["EXPENSE"]);
			});
		};
		$rootScope.$on("income-vs-expenses-received", function() {
			$scope.recalculateIncomeVsExpenses();
		});
	}]);

})(angular);