(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard");

	dashboard.controller("OssmoneyDashboardExpensesController", ["$rootScope", "$scope", "$translate", function($rootScope, $scope, $translate) {
		$scope.recalculateExpensesByCategory = function() {
			$scope.data_expenses = [];
			$scope.labels_expenses = [];
			angular.forEach(Object.keys($rootScope.expensesByCategory), function(category) {
				$scope.labels_expenses.push($translate.instant(category));
				$scope.data_expenses.push($rootScope.expensesByCategory[category].balance);
			});
		};
		$rootScope.$on("expenses-changed", function() {
			$scope.recalculateExpensesByCategory();
		});
	}]);

})(angular);