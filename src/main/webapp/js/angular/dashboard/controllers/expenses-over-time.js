(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard");

	dashboard.controller("OssmoneyDashboardExpensesOverTimeController", ["$rootScope", "$scope", function($rootScope, $scope) {
		$scope.recalculateExpensesByCategory = function() {
			var index = 0;
			var series = {};
			$scope.data_eot = [];
			$scope.labels_eot = [];
			$scope.series_eot = [];
			angular.forEach($rootScope.expensesByCategoryOverTime, function(monthData) {
				$scope.labels_eot.push(monthData.month);
				angular.forEach(Object.keys(monthData), function(category) {
					if (category !== "month") {
						if (series[category] === undefined) {
							series[category] = monthData[category].tag;
						}
					}
				});
			});
			angular.forEach(Object.keys(series), function(serie, index) {
				if (serie !== "month") {
					$scope.data_eot.push([]);
					$scope.series_eot.push(series[serie]);
					angular.forEach($rootScope.expensesByCategoryOverTime, function(monthData) {
						$scope.data_eot[index].push(monthData[serie] ? monthData[serie].balance : 0);
					});
				}
			});
		};
		$rootScope.$on("expenses-changed", function() {
			$scope.recalculateExpensesByCategory();
		});
	}]);

})(angular);