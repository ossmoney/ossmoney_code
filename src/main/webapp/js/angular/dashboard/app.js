(function(angular) {

	var dashboard = angular.module("ossmoneyDashboard", ["ui.bootstrap", "chart.js", "ngResource", "pascalprecht.translate"]);

	dashboard.run(["$rootScope", function($rootScope) {
		$rootScope.netWorth = 0;
		$rootScope.$on("accounts-changed", function() {
			$rootScope.netWorth = $rootScope.accounts.reduce(function(previousValue, account) {
				return previousValue + (account.creditCard ? account.currentBalance.number - account.initialBalance.number : account.currentBalance.number);
			}, 0);
			$rootScope.$broadcast("networth-changed");
		});
	}]);

	dashboard.run(["$rootScope", "$translate", "ExpensesService", function($rootScope, $translate, ExpensesService) {
		$rootScope.maxMonthsLoaded = 0;
		$rootScope.$on("income-vs-expenses-received", function() {
			var date = new Date();
			$rootScope.timeline = [];
			$rootScope.expensesByCategory = {};
			$rootScope.expensesByCategoryOverTime = [];
			var currentMonth = date.getMonth();
			var today = date.getFullYear() * 12 * 30 + (currentMonth + 1) * 30 + date.getDate();
			for (var i = 0; i <= $rootScope.incomeVsExpensesFromDate; i++) {
				var label = currentMonth - i + (currentMonth - i < 0 ? 12 : 0);
				$rootScope.timeline.unshift({
					"INCOME": 0,
					"EXPENSE": 0,
					"month": $translate.instant("month." + label)
				});
				$rootScope.expensesByCategoryOverTime.unshift({
					"month": $translate.instant("month." + label)
				});
			}
			angular.forEach($rootScope.incomeVsExpenses, function(transaction) {
				if (ExpensesService.inInterval(transaction.operationDate, today, $rootScope.incomeVsExpensesFromDate)) {
					ExpensesService.groupByMonth($rootScope, transaction, currentMonth + 1);
					if (transaction.transactionType === "EXPENSE") {
						ExpensesService.categorize($rootScope, $translate, transaction);
						ExpensesService.categorizeByTime($rootScope, $translate, transaction, currentMonth + 1);
					}
				}
			});
			$rootScope.$broadcast("expenses-changed");
		});
	}]);

	dashboard.run(["$rootScope", "IncomeVsExpenses", function($rootScope, IncomeVsExpenses) {
		$rootScope.$on("interval-changed", function(event, params) {
			$rootScope.incomeVsExpensesFromDate = params.months;
			if (($rootScope.incomeVsExpenses === undefined) | (params.months > $rootScope.maxMonthsLoaded)) {
				$rootScope.maxMonthsLoaded = params.months;
				var movements = IncomeVsExpenses.query({
					"months": params.months
				}, function() {
					$rootScope.incomeVsExpenses = movements;
					$rootScope.$broadcast("income-vs-expenses-received");
				});
			} else {
				$rootScope.$broadcast("income-vs-expenses-received");
			}
		});
	}]);

})(angular);